\section{Numeri finiti}

\begin{frame}{Numeri finiti}
	\begin{block}{Rappresentare i numeri in memoria}
		Tutti i numeri che utilizziamo hanno un numero \emph{finito}
		di cifre e sono rappresentati in memoria con la seguente codifica:
		\begin{center}
		\begin{tabular}{|c|p{3cm}|p{4cm}|}
			\hline
			\scriptsize{$\pm$} & \scriptsize{esponente ($p$ cifre)} & \scriptsize{mantissa ($t$ cifre)} \\
			\hline
		\end{tabular}
		\end{center}
	\end{block}
	%~ \begin{block}{}
	\footnotesize
	Chiamiamo $F(\beta,t,L,U)\subset\mathbb{R}$ l'insieme di tutti
	i reali rappresentabili nel calcolatore, dove:
	\begin{itemize}
		\item $L$: valore minimo che può assumere l'esponente;
		\item $U$: valore massimo che può assumere l'esponente;
		\item $\beta$: base utilizzata;
		\item $t$: numero di bit per la mantissa.
	\end{itemize}
	La cardinalità di $F(\beta,t,L,U)$ è pari a: $2(\beta-1)\beta^{t-1}\cdot(U-L+1)$
	%~ \end{block}
\end{frame}

%-------------SubSezione: Precisione di macchina
\subsection{Precisione di macchina}

\begin{frame}
	\begin{exampleblock}{Esercizio 1}
		Realizzare una m-function che calcoli la precisione della macchina
		su cui lavorate.
	\end{exampleblock}
	\vspace{0.3cm}

	\riq{
		\source{octave}{../es1/precisione.m}
	}
	
	\begin{block}{}
		La precisione da noi rilevata lavorando con variabili a 64 bit (doppia
		precisione) è: \texttt{2.22044604925031e-16}.
	\end{block}
\end{frame}

%-------------SubSezione: Valutazione polinomio
\subsection{Valutazione di un polinomio in un punto}

\begin{frame}
	\begin{exampleblock}{Esercizio 2}
		Realizzare una m-function che mediante lo
		schema di Horner valuti i seguenti polinomi su un set di punti e li
		rappresenti graficamente.  Confrontarne i tempi di calcolo con
		quelli ottenuti mediante una m-function che utilizzi l’algoritmo
		banale per la valutazione del polinomio.\\[0.2cm]
		\begin{tabular}{ p{7.5cm} l }
			\scriptsize{$p(x)=x^6-6x^5+15x^4-20x^3+15x^2-6x+1$} & \scriptsize{$1000$ punti in $[0.9,1.1]$} \\
			\scriptsize{$p(x)= x^8 - 8 x^7 + 28 x^6 -7 x^4 +12 x^3 -4 x +1$} & \scriptsize{$1000$ punti in $[-1,1]$} \\
		\end{tabular}
	\end{exampleblock}
	\vspace{0.3cm}
	\riq{
		\source{octave}{../es1/valuta_ingenuo.m}
	}
	\vspace{0.3cm}
\end{frame}

\begin{frame}
		\riq{
			\source{octave}{../es1/valuta_horner.m}
		}
	\begin{block}{}
		L'algoritmo ingenuo esegue $n$ addizioni e $2n$ prodotti invece l'algoritmo
		di Horner esegue $n$ addizioni ed $n$ prodotti:
		$$ p(x) = a_0 + x(a_1 + x(a_2 + \cdots + x(a_{n-1} + a_n x)\cdots)) $$
		Facendo infatti una media dei tempi di valutazione notiamo una
		differenza di velocità:
		\begin{itemize}
			\item Algoritmo ingenuo, in media $\simeq 78.12\, \mu s$;
			\item Algoritmo di Horner, in media $\simeq 75.82\, \mu s$.
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Funzione 1}
		$p(x)=x^6-6x^5+15x^4-20x^3+15x^2-6x+1$ in $[0.9,1.1]$
	\end{block}
	\input{../es1/plot01.tex}
\end{frame}
\begin{frame}
	\begin{block}{Funzione 2}
		$p(x)= x^8 - 8 x^7 + 28 x^6 -7 x^4 +12 x^3 -4 x +1$ in $[-1,1]$
	\end{block}
	\input{../es1/plot02.tex}
\end{frame}

\begin{frame}
	\begin{exampleblock}{Esercizio 3}
		Realizzare una funzione Matlab che calcola il valore del polinomio 
		$p(x) = (x - 1)^6$ utilizzando le formule:
		\begin{itemize}
			\item $p_1(x) = x^6 - 6 x^5 + 15 x^4 - 20 x^3 + 15 x^2 - 6 x +1$
			\item $p_2(x) = (x - 1)^6$
			\item con lo schema di Horner.
		\end{itemize}
		e ne realizza il grafico in $[1-d, 1+d]$, per:
		$$d=0.1, 0.01, 0.008, 0.007, 0.005, 0.003$$
		Cosa succede al diminuire di $d$?
	\end{exampleblock}
\end{frame}

\begin{frame}
	\only<1-1>{
		\input{../es1/plot1.tex}
	}
	\only<2-2>{
		\input{../es1/plot2.tex}
	}
	\only<3-3>{
		\input{../es1/plot3.tex}
	}
	\only<4-4>{
		\input{../es1/plot4.tex}
	}
	\only<5-5>{
		\input{../es1/plot5.tex}
	}
	\only<6-6>{
		\input{../es1/plot6.tex}
	}
\end{frame}

\begin{frame}
	\begin{block}{Errori...}
		Notiamo che sia l'algoritmo ingenuo che quello di Horner commettono degli
		errori nella valutazione che non sussistono invece nella forma
		concisa del polinomio: questa, chiaramente, esegue il minimo
		numero di operazioni in virgola mobile ed è per questo la migliore in
		quanto a stabilità.
	\end{block}
	\begin{block}{Fenomeno della cancellazione}
		Gli errori che abbiamo visto sono causati dal \textit{fenomeno della
		cancellazione} di cifre significative. Con tale termine ci riferiamo infatti
		alla conseguenza più grave della rappresentazione con precisione
		finita dei numeri reali nei calcolatori. Il fenomeno si verifica quando si
		esegue una differenza tra due numeri che hanno molte delle prime cifre uguali,
		infatti, nel nostro caso si presenta quando i numeri cominciano ad essere
		molto piccoli.
	\end{block}
\end{frame}

%-------------SubSezione: e^x
\subsection{Calcolo di \texorpdfstring{$e^x$}{Lg}}

\begin{frame}
	\begin{exampleblock}{Esercizio 4}
		Realizzare una m-function che preso in
		input un valore di $x$, approssimi l'esponenziale $e^x$ mediante un
		troncamento ad $N$ termini dello sviluppo in serie di
		Taylor. Calcolate il valore esatto facendo uso della funzione di
		Matlab \texttt{exp(x)}.
	\end{exampleblock}
	\begin{block}{Espansione in serie di Taylor}
		La \emph{funzione esponenziale} è definibile in diversi
		modi: una definizione utile per approssimarne il valore con un
		algoritmo iterativo è la sua espansione in serie di Taylor:
		$$ e^x = \sum\limits_{i=0}^\infty{\frac{x^i}{i!}} =
		1+x+\frac{x^2}{2}+\frac{x^3}{6}+\frac{x^4}{24}+\dots $$
	\end{block}
\end{frame}

\begin{frame}
	\riq{
		\source{octave}{../es1/exp_instabile.m}
	}
	\begin{block}{...e se $x<0$ ?}
		Il codice sopra funziona bene per tutti gli argomenti positivi, ma
		provando ad eseguirlo con argomenti negativi notiamo che
		nella sommatoria i termini in posizione pari sono positivi mentre
		quelli in posizione dispari diventano negativi: si creano quindi le
		condizioni necessarie per il solito fenomeno della cancellazione.
		Un modo per ``limitare'' le conseguenze è raggruppare i termini
		\textit{in base al segno}, sommarli e alla fine eseguire un unica
		differenza.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Evitiamo tutte le sottrazioni}
		Notiamo però che in questo caso possiamo evitare del tutto le 
		sottrazioni calcolando $e^x$ come il reciproco di $e^{-x}$.
		La funzione ora è riscritta in modo da essere numericamente stabile:
	\end{block}
	\vspace{0.3cm}
	\riq{
		\source{octave}{../es1/exp_stabile.m}
	}
\end{frame}

\begin{frame}
	\begin{block}{Tabella degli errori relativi}
		\vspace{0.3cm}
		{\centering
		\resizebox{\linewidth}{!}{
			\begin{tabular}{|c|c|c|c|c|c|}
			\hline
				x & \texttt{exp(x)} & \texttt{exp\_instabile(x)} & $\leftarrow$
				Errore relativo & \texttt{exp\_stabile(x)} & $\leftarrow$ Errore
				relativo \\
			\hline
			\csvreader[late after line=\\ \hline]
			{../es1/exp.dat} % Il nome del file
			{a=\a,b=\b,c=\c,d=\d,e=\e,f=\f}
			{\texttt{\a} & \texttt{\b} & \texttt{\c} & \texttt{\d} & \texttt{\e} & \texttt{\f}}
			\end{tabular}
		}}
		\vspace{0.2cm}
	\end{block}
	\begin{block}{}
		Come volevasi dimostrare, per gli argomenti negativi notiamo un
		incremento dell'errore relativo commesso dall'algoritmo numericamente
		instabile: ovvero, man mano che l'argomento diminuisce, i risultati
		prodotti sono sempre meno accurati. Al contrario, la versione stabile
		ottiene risultati molto vicini al valore reale anche per $x < 0$.
	\end{block}
\end{frame}

%-------------SubSezione: Calolo derivata
\subsection{Calcolo della derivata}

\begin{frame}
	\begin{exampleblock}{Esercizio 5}
		  Realizzare una m-function che approssimi
		la derivata delle seguenti funzioni nel punto specificato al
		diminuire di $h$:\\[0.2cm]
		\begin{tabular}{ p{6cm} l }
			$f(x)=2x^3 +3x^2 +2x+1$ & in $x=1$ \\
			$f(x)=\sin x$ & in $x=\frac\pi{6}$ \\
		\end{tabular}
	\end{exampleblock}
	\vspace{0.3cm}
	\pause
	\riq{
		\source{octave}{../es1/derivate.m}
	}
\end{frame}

\begin{frame}
	\begin{block}{Tabella degli errori relativi}
	\begin{center}
	\footnotesize
	\begin{tabular}{p{5.3cm} p{5.3cm}}
		\scriptsize{$\quad f(x)=2x^3 +3x^2 +2x+1$} & \scriptsize{$\quad f(x)=\sin x$}\\
		\scriptsize{$\quad f'(1) = 14$}\vspace{0.2cm} & \scriptsize{$\quad f'\left(\frac\pi6\right)\simeq 0.86603$}\\
		\resizebox{\linewidth}{!}{
			\begin{tabular}{|c|c|c|}
			\hline
				h & valore calcolato & errore relativo \\
			\hline
			\csvreader[late after line=\\ \hline]
			{../es1/es4_1.dat} % Il nome del file
			{a=\a,b=\b,c=\c,d=\d}
			{\texttt{\a} & \texttt{\b} & \texttt{\d}}
			\end{tabular}
		}
		&
		\resizebox{\linewidth}{!}{
			\begin{tabular}{|c|c|c|}
			\hline
				h & valore calcolato & errore relativo \\
			\hline
			\csvreader[late after line=\\ \hline]
			{../es1/es4_2.dat} % Il nome del file
			{a=\a,b=\b,c=\c,d=\d}
			{\texttt{\a} & \texttt{\b} & \texttt{\d}}
			\end{tabular}
		}
	\end{tabular}
	\end{center}
	\end{block}
\end{frame}

%~ \begin{frame}
	%~ \begin{block}{}
		%~ Osserviamo che il valore calcolato con il nostro algoritmo cresce al diminuire 
		%~ di $h$ fino ad un certo punto e poi, invertendo la sua tendenza,
		%~ la precisione del valore ottenuto invece di aumentare diminuisce.
%~ 
		%~ Ciò accade perché quando $h$ inizia a diventare molto piccolo i due termini
		%~ al numeratore (nell'algoritmo) sono quasi uguali e l'operazione di 
		%~ differenza provoca la cancellazione di cifre significative.
	%~ \end{block}
%~ \end{frame}

\subsection{Approssimazione del valore di \texorpdfstring{$\pi$}{Lg}}

\begin{frame}
	\begin{exampleblock}{Esercizio 6}
		Realizzate 4 m-function che implementino i
		4 metodi di approssimazione del valore di $\pi$ visti a lezione:
		metodo di Archimede, metodo di Viete, metodo di Leibniz, metodo
		dello sviluppo in serie dell'arcoseno. Confrontate la stabilità e la
		velocità di questi algoritmi, mediante un grafico che riporti
		l'errore relativo al variare di $n$.
	\end{exampleblock}
	
	\input{../es1/plot7.tex}
\end{frame}

\begin{frame}
	\begin{block}{Metodo di Archimede}
		Questo metodo si basa sul calcolo del limite per $n$ che tende a
		$\infty$ dell'area di un poligono regolare con	$2^n$ lati inscritto
		in una circonferenza di raggio 1.
	\end{block}
	\vspace{0.3cm}
	\begin{columns}
		\begin{column}{4cm}
			\includegraphics[width=4cm]{../es1/polyg}
		\end{column}
		\begin{column}{8cm}
		\riq{
			\source{octave}{../es1/pi1.m}
		}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\begin{block}{Metodo di Viete}
		Questo metodo si basa su una rappresentazione di $\pi$ mediante la
		produttoria infinita:
		$$ \frac2\pi= \frac{\sqrt2}2\cdot \frac{\sqrt{2+\sqrt2}}2\cdot
		\frac{\sqrt{2+\sqrt{2+\sqrt2}}}2\cdots $$
	\end{block}
	\vspace{0.3cm}
	\riq{
		\source{octave}{../es1/pi2.m}
	}
\end{frame}

\begin{frame}
	\begin{block}{Metodo di Leibniz}
		Questo metodo si basa su una rappresentazione di $\pi$ mediante la
		sommatoria infinita:
		$$ \frac{\pi}{4} = \sum_{n=0}^\infty \, \frac{(-1)^n}{2n+1} =
		1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \frac{1}{9} - \cdots $$
	\end{block}
	\riq{
		\source{octave}{../es1/pi3.m}
	}
\end{frame}

\begin{frame}
	\begin{block}{Metodo di sviluppo dell'arcoseno}
		Quest'ultimo metodo si basa sul fatto che $\sin\left(\frac{\pi}{6}
		\right)=\frac12$ e quindi sullo sviluppo dell'arcoseno di $\frac12$.
		Ha una buona rapidità di convergenza: 5 nuovi termini producono almeno
		3 nuove cifre corrette.
	\end{block}
	\riq{
		\inputminted[fontsize=\scriptsize,linenos=true,tabsize=4]{octave}{../es1/pi4.m}
	}
\end{frame}
