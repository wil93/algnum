\section{Metodi diretti per la soluzione di sistemi lineari}

\begin{frame}{Metodi diretti per la soluzione di sistemi lineari}
	\begin{block}{Cosa sono}
		Sono metodi che forniscono la soluzione esatta di un sistema lineare
		attraverso un numero finito di passi. Il principio di base di tali
		metodi consiste nel trasformare il sistema originale (operando
		modifiche sulla matrice associata) in un sistema a risoluzione
		immediata.
	\end{block}
	\vspace{0.3cm}
	%~ \begin{block}{}
		In particolare analizzeremo:
		\begin{itemize}
			\item sistemi triangolari superiori,
			\item sistemi triangolari inferiori,
			\item sistemi tridiagonali.
		\end{itemize}
		È necessario quindi introdurre il concetto di \textbf{matrice
		triangolare}.
	%~ \end{block}
\end{frame}

%-------------SubSezione: Matrici triangolari
\subsection{Matrici triangolari}
\begin{frame}
	\begin{block}{Matrice triangolare}
		Una matrice è triangolare se ha elementi diversi da 0 solamente
		sulla diagonale e nelle posizioni ad essa superiori (triangolare
		superiore) oppure inferiori (triangolare inferiore).\\
		$$
		\left[ \begin{array}{cccc}
			a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
			0 & a_{2,2} & \cdots & a_{2,n} \\
			\vdots  & \vdots  & \ddots & \vdots  \\
			0 & 0 & \cdots & a_{m,n}
		\end{array} \right] \quad
		\left[ \begin{array}{cccc}
			a_{1,1} & 0 & \cdots & 0 \\
			a_{2,1} & a_{2,2} & \cdots & 0 \\
			\vdots  & \vdots  & \ddots & \vdots  \\
			a_{m,1} & a_{m,2} & \cdots & a_{m,n}
		\end{array} \right]
		$$
	\end{block}
	\vspace{0.3cm}
	%~ \begin{block}{}
		Risolvere un sistema triangolare è particolarmente semplice: è
		sufficiente infatti operare una sostituzione in avanti (se il sistema
		è triangolare inferiore) o all'indietro (se il sistema è triangolare
		superiore).
	%~ \end{block}
\end{frame}

\begin{frame}
	Algoritmo di sostituzione in avanti:
	\riq{
		\source{octave}{../es3/fsub.m}
	}\\
	\vspace{0.3cm}
	Algoritmo di sostituzione all'indietro:
	\riq{
		\source{octave}{../es3/bsub.m}
	}
\end{frame}

%-------------SubSezione: Fattorizzazione di Gauss
\subsection{Fattorizzazione di Gauss}
\begin{frame}
	\begin{block}{Fattorizzazione}
		L'idea di base dei metodi diretti consiste nel fattorizzare la 
		matrice $A$ di partenza nel prodotto di due matrici $L \times R$,
		dove $L$ è triangolare inferiore e $R$ è triangolare superiore.
	\end{block}
	\vspace{0.5cm}
	%~ \begin{block}{}
		Tale fattorizzazione permette di risolvere un sistema lineare 
		$$Ax = b \quad \Rightarrow \quad LRx = b$$
		decomponendolo in due sistemi lineari più semplici, posto $y = Rx$,
		si ha $Ly = b$, quindi basta risolvere i due sistemi triangolari:
		$$
		\begin{cases}
			Ly = b\\
			Rx = y
		\end{cases}
		$$
	%~ \end{block}
\end{frame}

\begin{frame}
	\begin{block}{Fattorizzazione di Gauss}
		Il metodo di fattorizzazione di Gauss realizza la seguente fattorizzazione 
		della matrice $A$:
		$$PA = LR$$
		dove $P$ è una matrice di permutazione che tiene conto degli eventuali 
		scambi di righe, $L$ è una matrice triangolare inferiore che contiene i 
		moltiplicatori e con elementi sulla diagonale uguali a 1, e $R$ è la 
		matrice triangolare superiore ottenuta dall'eliminazione gaussiana.
	\end{block}
	\vspace{0.3cm}
	L'algoritmo di Gauss da noi implementato cerca ad ogni iterazione
	il pivot di valore massimo, tra quelli possibili nella colonna.\\
	\vspace{0.3cm}
	Questo, come vedremo più avanti, ci permette di migliorare in parte
	la stabilità dell'algoritmo.
\end{frame}

\begin{frame}
	Algoritmo di fattorizzazione di Gauss con pivotaggio a perno massimo:
	\riq{
		\source{octave}{../es3/gauss.m}
	}
	\begin{block}{Complessità computazionale}
		\footnotesize
		La complessità computazionale dell'algoritmo è
		$\ogrande\left(\frac{1}{3} n^3\right)$.
	\end{block}
\end{frame}

%-------------SubSezione: Stabilità numerica
\subsection{Stabilità numerica}
\begin{frame}[t]
	%~ \begin{block}{Analisi all'indietro}
		Seguendo la filosofia dell'\textit{analisi all'indietro} introdotta
		da Wilkinson possiamo affermare che la perturbazione su una matrice
		$A$ non dipende solamente dalle perturbazioni sulle matrici $L$ e $R$
		ma anche dalle matrici stesse.

		Quindi possiamo definire la stabilità numerica di un algoritmo di
		fattorizzazione $LR$ in base alle matrici $L$ e $R$.
	%~ \end{block}
	\begin{block}{Stabilità degli algoritmi di fattorizzazione $LR$}
		Si dice che un algoritmo produce una fattorizzazione $LR$ stabile
		\textbf{in senso forte} di una matrice $A$, con elementi minori o uguali a 1,
		se possiamo trovare delle costanti positive $a$ e $b$, \textbf{indipendenti
		dall'ordine e dagli elementi} di $A$, tali che:
		$$|l_{ij}| \le a \quad |r_{ij}| \le b$$
		
		Se $a$ e $b$ dipendono dall'ordine di $A$ si dice che la fattorizzazione
		è stabile \textbf{in senso debole}.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Stabilità dell'algoritmo di Gauss}
		Verificando la stabilità dell'algoritmo di Gauss ci accorgiamo che
		scegliendo sempre il perno massimo otteniamo una matrice $L$ con
		elementi minori o uguali a 1, quindi possiamo dire che
		$a = 1$.\\\vspace{0.3cm}

		Al contrario, gli elementi di $R$ non possono essere limitati
		superiormente da una costante ma crescono esponenzialmente con $n$:
		$$b = \mathrm{max}|a_{ij}| 2^{n-1}$$
	\end{block}

	\begin{block}{In conclusione...}
		L'algoritmo di Gauss è in generale stabile in senso debole perchè
		$b$ dipende da $n$.
	\end{block}
\end{frame}

%-------------SubSezione: Matrice di Wilkinson
\subsection{Matrice di Wilkinson}
\begin{frame}
	\begin{block}{Matrice di Wilkinson}
		E' una matrice costruita appositamente per raggiungere la maggiorazione
		di $b$ sui valori della matrice $R$ operando su di essa la fattorizzazione
		di Gauss.

		Alla fine della fattorizzazione infatti avremo che $r_{n,n} = 2^{n-1}$
		considerando che la matrice di Wilkinson ha $\mathrm{max}|a_{ij}| = 1$.
	\end{block}
	\begin{block}{Esempio di ordine 6}
		$$
		A = \left[ \begin{array}{cccccc}
		1 & 0 & 0 & 0 & 0 & -1\\
		1 & 1 & 0 & 0 & 0 & 1\\
		-1 & 1 & 1 & 0 & 0 & -1\\
		1 & -1 & 1 & 1 & 0 & 1\\
		-1 & 1 & -1 & 1 & 1 & -1\\
		1 & -1 & 1 & -1 & 1 & 1\end{array} \right]
		$$
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 1
\subsection{Esercizio 1}
\begin{frame}
	\begin{exampleblock}{Esercizio 1}
		 Realizzate uno script Matlab che :
		\begin{enumerate}
			\item Costruisca la matrice di Wilkinson di ordine $n=12$.
			\item Utilizzi la funzione \texttt{spy(A)} per visualizzare la
				struttura della matrice.
			\item Ne realizzi la fattorizzazione di Gauss con pivotaggio a perno
				massimo per colonne.
			\item Effettui una perturbazione del 5\% sull'elemento di posizione
				$(n,n)$ della matrice $R$, Sia $R_{p}$ la matrice $R$ con
				l'elemento $R(n,n)$ perturbato.
			\item Effettui il prodotto $A_{p} = L \times R_{p}$ e stimi l'errore
				relativo commesso sull'elemento $A_{p}(n,n)$, rispetto al valore
				esatto $A(n,n)$.
		\end{enumerate}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{Matrice di Wilkinson di ordine 12}
		$$
		A = \left[ \begin{array}{cccccccccccc}
		1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & -1\\
		1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\
		-1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & -1\\
		1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\
		-1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & -1\\
		1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 1\\
		-1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & -1\\
		1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 1\\
		-1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & -1\\
		1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 1\\
		-1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & -1\\
		1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1\end{array} \right]
		$$
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{\texttt{spy(A)}}
		\input{../es3/spyplot1.tex}
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Dopo la fattorizzazione di Gauss}
		$$\tiny{
		L = \left[ \begin{array}{cccccccccccc}
		1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
		1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
		-1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
		1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
		-1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
		1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\
		-1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 0 & 0\\
		1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & 0 & 0\\
		-1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 0 & 0\\
		1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & 0 & 0\\
		-1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1 & 0\\
		1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & -1 & 1 & 1\end{array} \right],}
		$$$$\tiny{
		R = \left[ \begin{array}{cccccccccccc}
		1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & -1\\
		0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2\\
		0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & -4\\
		0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 8\\
		0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & -16\\
		0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 32\\
		0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & -64\\
		0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 128\\
		0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & -256\\
		0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 512\\
		0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & -1024\\
		0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & \textbf{2048}\end{array} \right],
		\quad P = I_{12}}
		$$
	\end{block}
\end{frame}

\begin{frame}
	\begin{alertblock}{Elemento $R(n,n)$}
		Come ci aspettavamo $R(n,n)$ è uguale alla costante $b = 2^{n-1} = 2048$.
	\end{alertblock}
	\begin{block}{Perturbazione}
		Effettuiamo ora la perturbazione sull'elemento $r_{n,n}$ ottenendo
		la matrice $A_p$ e vediamo qual è l'errore relativo tra
		$A(n,n)$ e $A_p(n,n)$.
		\vspace{0.3cm}

		\source{octave}{../es3/script1_es.m}
	\end{block}
\end{frame}

%-------------SubSezione: Condizionamento di un sistema lineare
\subsection{Condizionamento di un sistema lineare}
\begin{frame}
	\begin{block}{Condizionamento di un sistema lineare}
		La stabilità dell'algoritmo di fattorizzazione non è l'unico fattore che influenza
		soluzione $x$ di un sistema lineare, è infatti importante considerare
		anche l'influenza che hanno le perturbazioni sulla matrice $A$ e sul
		termine noto $b$.
	\end{block}
	\begin{block}{Perturbazioni}
		Le perturbazioni sono essenzialmente delle approssimazioni che
		scaturiscono dal fatto che il sistema reale deve essere rappresentato
		nel calcolatore con numeri finiti ed che le operazioni che effettuiamo
		vengono eseguite in aritmetica finita.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Indice di condizionamento}
		\footnotesize
		Possiamo calcolare un indicatore sul sistema che ci dice se esso
		è ben condizionato oppure no:
		$$K(A) = \|A\| \cdot \|A^{-1}\|$$
		$K(A)$ è chiamato \textbf{indice di condizionamento} ed in generale
		può assumere valori maggiori o uguali a 1.
	\end{block}
	\begin{block}{Se $K(A) \gg 1$}
		La matrice $A$ è mal condizionata e il problema $Ax = b$ è mal posto.
		Errori anche molto piccoli in $A$ e/o in $b$ possono trasformarsi in
		errori molto grandi sulla soluzione finale.
	\end{block}
	\begin{block}{Se $K(A) \simeq 1$}
		La matrice $A$ è ben condizionata e $Ax = b$ è ben posto.
		Piccoli errori presenti in $A$ e/o in $b$ non vengono amplificati
		sulla soluzione del sistema.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Esempi di matrici mal condizionate}
		Le matrici di Vandermonde e di Hilbert sono due esempi conosciuti
		di matrici mal condizionate.
	\end{block}
	\begin{block}{Matrice di Vandermonde}
		È una matrice di dimensione $n \cdot n$ il cui elemento
		$$a_{ij} = (x_i)^j \qquad i,j = 0, \dots, n-1$$
		dato $x$ un vettore di $n$ elementi qualsiasi.
	\end{block}
	\begin{block}{Matrice di Hilbert}
		È una matrice di dimensione $n \cdot n$ il cui elemento
		$$a_{ij} = \frac{1}{i + j - 1} \qquad i,j = 0, \dots, n-1$$
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 2
\subsection{Esercizio 2}
\begin{frame}
	\begin{exampleblock}{Esercizio 2}
		Sia dato il sistema lineare $$Ax=b$$ con
		$A$ matrice di Hilbert di ordine $n$ e $b$ termine noto la cui
		$i$-esima componente è data da $$b_{i} = \sum_{j=1}^{n}a_{i,j} \qquad
		i=1,\dots,n$$
		\begin{enumerate}
			\item Risolvete il sistema lineare in questione al variare di $n$
			tra 2 e 15 e calcolate l'errore relativo della soluzione calcolata
			con l'aritmetica finita rispetto a quella calcolata con
			l'aritmetica reale.
			\item Calcolare l'indice di condizionamento di $A$ al crescere di
			$n$.
		\end{enumerate}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{Indice di condizionamento al crescere di $n$}
		\begin{center}
			\footnotesize
			\begin{tabular}{|c|c|c|}
				\hline
				n & errore relativo & indice di condizionamento\\
				\hline
				\csvreader[late after line=\\ \hline]
				{../es3/es2.dat} % Il nome del file
				{a=\a,b=\b,c=\c}
				{\texttt{\a} & \texttt{\b} & \texttt{\c}}
			\end{tabular}
		\end{center}
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 3
\subsection{Esercizio 3}
\begin{frame}
	\begin{exampleblock}{Esercizio 3}
		\footnotesize
		Sia dato il sistema lineare $$Ax=b$$
		con $A$ matrice di Vandermonde di ordine $n$ generata dal vettore $x$
		costituito da $n$ valori equispaziati nell'intervallo $[0,1]$ e $b$
		termine noto la	cui $i$-esima componente è data da
		$$b_{i} = \sum_{j=1}^{n}a_{i,j} \qquad i=1,\dots,n$$
		\begin{enumerate}
			\item Risolvete il sistema lineare in questione al variare di $n$
			tra 2 e 15 e calcolate l'errore relativo della soluzione calcolata
			con l'aritmetica finita rispetto a quella calcolata con
			l'aritmetica reale.
			\item Calcolare l'indice di condizionamento di $A$ al crescere di
			$n$.
			\item Ripetere lo stesso esperimento con il caso in cui il vettore
			$x$ sia costituito da $n$ elementi equispaziati nell'intervallo
			$[1,100]$.
		\end{enumerate}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{Indice di condizionamento al crescere di $n$}
		\begin{columns}
		\begin{column}{6.1cm}
		\begin{center}
		\texttt{linspace(0, 1, n)}\\
		\resizebox{1\linewidth}{!}{
			\begin{tabular}{|c|c|c|}
				\hline
				n & \texttt{errore relativo} & \texttt{indice di condizionamento}\\
				\hline
				\csvreader[late after line=\\ \hline]
				{../es3/es3_1.dat} % Il nome del file
				{a=\a,b=\b,c=\c}
				{\texttt{\a} & \texttt{\b} & \texttt{\c}}
			\end{tabular}
		}
		\end{center}
		\end{column}
		
		\begin{column}{6.1cm}
		\begin{center}
		\texttt{linspace(1, 100, n)}\\
		\resizebox{1\linewidth}{!}{
			\begin{tabular}{|c|c|c|}
				\hline
				n & \texttt{errore relativo} & \texttt{indice di condizionamento}\\
				\hline
				\csvreader[late after line=\\ \hline]
				{../es3/es3_2.dat} % Il nome del file
				{a=\a,b=\b,c=\c}
				{\texttt{\a} & \texttt{\b} & \texttt{\c}}
			\end{tabular}
		}
		\end{center}
		\end{column}
		\end{columns}
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 4
\subsection{Esercizio 4}
\begin{frame}
	\begin{exampleblock}{Esercizio 4}
		Costruire uno script Matlab che risolva il sistema lineare $Ax=b$,
		dove $A=\mathtt{hilb(10)}+\mathtt{eye(10)}$, essendo \texttt{eye(10)}
		la matrice identità di ordine 10. $b$ è il termine noto scelto in maniera
		tale che la soluzione del sistema lineare sia il vettore unitario.
		\begin{enumerate}
			\item Calcolare l'indice di condizionamento della matrice e l'errore
			relativo sulla soluzione.
			\item Perturbare l'elemento $b_1$ del termine noto dell'1\% e
			risolvere nuovamente il sistema lineare.
			\item Calcolare l'errore relativo sulla soluzione e confrontarlo con
			l'errore relativo sul termine noto.
		\end{enumerate}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{Sistema non perturbato}
		\source{octave}{../es3/script4_1.m}
	\end{block}
	\begin{block}{Sistema perturbato}
		\source{octave}{../es3/script4_2.m}
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 5
\subsection{Esercizio 5}
\begin{frame}
	\begin{exampleblock}{Esercizio 5}
	  Costruire uno script Matlab che risolva il sistema lineare $Ax=b$, dove
	  $$
	  A = \left[ \begin{array}{cccc}
		10 & 1 & 4 & 0\\
		1 & 10 & 5 & -1\\
		4 & 5 & 10 & 7\\
		0 & -1 & 7 & 9\end{array} \right], \quad
		b = \left[ \begin{array}{c} 15\\ 15\\ 26\\ 15 \end{array} \right]
	  $$
	  \begin{enumerate}
	  \item Calcolare l'indice di condizionamento della matrice $A$ e
		risolvere il sistema lineare facendo uso della fattorizzazione di
		Gauss con pivotaggio a perno massimo per colonne.
	  \item Perturbare l'elemento $b_1$ del termine noto dell'1\% e
		risolvere nuovamente il sistema lineare.
	  \item Calcolare l'errore relativo sulla soluzione e confrontarlo con
		l'errore relativo sul termine noto.
	  \end{enumerate}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{Soluzione esercizio 5}
		\source{octave}{../es3/script5_1.m}
	\end{block}
\end{frame}

%-------------SubSezione: Matrici tridiagonali
\subsection{Matrici tridiagonali}
\begin{frame}
	\begin{block}{Matrici tridiagonali}
		Le matrici tridiagonali sono costituite da tutti zero tranne sulla 
		diagonale principale e sulle due codiagonali.

		Una matrice tridiagonale $A$ può essere fattorizzata nel prodotto
		di due matrici, $L$  bidiagonale inferiore ed $R$ bidiagonale
		superiore.

		Possiamo quindi modificare l'algoritmo di Gauss in modo da ottimizzarlo
		nel fattorizzare matrici tridiagonali.
	\end{block}
	\begin{block}{Algoritmo di gauss per matrici tridiagonali}
		La nuova funzione prende in ingresso i 3 vettori, $a$ (diagonale principale),
		$b$ (codiagonale superiore) e $c$ (codiagonale inferiore), che rappresentano
		la matrice $A$.

		Restituisce i vettori $\beta$ e $\alpha$ che rappresentano rispettivamente
		la codiagonale inferiore di $L$ e la diagonale principale di $R$.

		La diagonale principale di $L$ è il vettore unitario mentre la
		codiagonale superiore di $R$  è lo stesso vettore $c$ della matrice $A$.
	\end{block}
\end{frame}

\begin{frame}
	\riq{
		\source{octave}{../es4/gauss_tri.m}
	}

	\begin{block}{Complessità computazionale}
		Questa versione dell'algoritmo di Gauss ha complessità computazionale
		lineare.
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 6
\subsection{Esercizio 6}
\begin{frame}
	\begin{exampleblock}{Esercizio 6}
		Sia $A$ una matrice tridiagonale, i cui valori diagonali sono 
		memorizzati nel vettore $a$, $a_i=2 \quad i=1,\dots n$, i valori della
		codiagonale superiore sono memorizzati nel vettore $b$, $b_i=-1 \quad i=2,\dots n$,
		e i valori della codiagonale superiore sono memorizzati nel vettore 
		$c$, $c_i=-1 \quad i=1,\dots n-1$. Realizzare uno script Matlab che risolva il
		sistema lineare $Ax=t$, dove $t$ è il termine noto scelto in maniera tale
		che la soluzione del sistema sia il vettore unitario, sia con il metodo
		di Gauss per matrici tridiagonali che con il metodo di Gauss per matrici
		piene, al variare dell'ordine $n$ della matrice.
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{Creazione matrice tridiagonale}
		\source{octave}{../es4/script6_1.m}
	\end{block}
	\begin{block}{Confronto tra i due algoritmi di Gauss}
		\begin{center}
		  \begin{tabular}{|c|c|c|}
		  \hline
			n & \texttt{tempo tridiagonale (s)} & \texttt{tempo piena (s)}\\
		  \hline
		  \csvreader[late after line=\\ \hline]
		  {../es4/es1.dat} % Il nome del file
		  {a=\a,b=\b,c=\c}
		  {\texttt{\a} & \texttt{\b} & \texttt{\c}}
		  \end{tabular}
		\end{center}
	\end{block}
\end{frame}

%-------------SubSezione: Matrici simmetriche e definite positive
\subsection{Matrici simmetriche e definite positive}
\begin{frame}
	\begin{block}{Matrici simmetriche e definite positive}
		\footnotesize
		Una matrice $A$ è simmetrica e definita positiva se: 
		$$ A = A^T \quad \text{e} \quad x^T A x > 0; \quad \quad \forall x \neq 0, \quad x \in \mathbb{R}^n $$

		Questa condizione è difficile da verificare, quindi utilizzeremo
		una condizione sufficiente affinchè una matrice simmetrica sia
		definita positiva, che risulta utile nella pratica.
	\end{block}
	\begin{block}{Condizione sufficiente}
		\footnotesize
		Sia $A \in \mathbb{R}^n$ simmetrica, se $A$ ha elementi diagonali
		ed è a diagonale strettamente dominante, cioè se:
		$$ |a_{ii}| > \sum_{\substack{j=1\\ j \ne i}}^{n} |a_{ij}|$$
		allora $A$ è definita positiva.
	\end{block}
\end{frame}

%-------------SubSezione: Fattorizzazione di Cholesky
\subsection{Fattorizzazione di Cholesky}
\begin{frame}
	\begin{block}{Teorema di Cholesky}
		Sia $A$ una matrice simmetrica e definita positiva allore esiste $L$ una
		matrice triangolare inferiore con elementi diagonali positivi tale che 
		$$A = L \times L^\intercal$$
	\end{block}
	\begin{block}{Fattorizzazione di Cholesky}
		L'algoritmo di fattorizzazione di Cholesky fattorizza la matrice
		$A$ sfruttando il teorema sopraccitato.

		Siccome la matrice $A$ è definita positiva siamo sicuri che i radicandi
		presenti nell'algoritmo risultano positivi. Questo ci suggerisce che
		possiamo usare l'algoritmo di Cholesky anche per controllare se
		la matrice $A$ è effettivamente simmetrica e definita positiva. 
	\end{block}
\end{frame}

\begin{frame}
	\riq{
		\source{octave}{../es4/cholesky.m}
	}
	\begin{block}{Complessità computazionale}
		\footnotesize
		La complessità è $\ogrande\left(\frac16 n^3\right)$, la metà di quella
		dell'algoritmo di Gauss per matrici piene.  
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 7
\subsection{Esercizio 7}
\begin{frame}
	\begin{exampleblock}{Esercizio 7}
		Costruire una matrice simmetrica di ordine $n=3000$, nel seguente modo:\\
		\qquad\texttt{A = rand(3000);}\\
		\qquad\texttt{B = A' * A;}\\
		Verificare con l'algoritmo di Fattorizzazione di Cholesky se la 
		matrice $B$ sia definita positiva. In caso positivo, risolvere il 
		sistema lineare $Bx=y$, con $y$ termine noto scelto in maniera tale 
		che la soluzione del sistema lineare sia il vettore unitario,
		\begin{itemize}
			\item Facendo uso della fattorizzazione di Cholesky.
			\item Facendo uso della fattorizzazione di Gauss.
		\end{itemize}
		Confrontare i tempi  dei due metodi per il calcolo della fattorizzazione.
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{$B$ è definita positiva?}
		\source{octave}{../es4/script7_1.m}
	\end{block}
	\begin{block}{Risolviamo il sistema lineare con Cholesky}
		\source{octave}{../es4/script7_2.m}
	\end{block}
\end{frame}
\begin{frame}
	\begin{block}{Risolviamo il sistema lineare con Gauss}
		\source{octave}{../es4/script7_3.m}
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 8
\subsection{Esercizio 8}
\begin{frame}
	\begin{exampleblock}{Esercizio 8}
		\footnotesize
		Costruire una matrice $A$ simmetrica e definita positiva seguendo 
		la condizione sufficiente affinché una matrice simmetrica sia 
		definita positiva. Risolvere il sistema lineare $Ax=b$, con $b$
		termine noto scelto in maniera tale che la soluzione sia il vettore
		unitario, mediante la fattorizzazione di Cholesky.
	\end{exampleblock}

	\begin{block}{Costruiamo una matrice simmetrica e definita positiva}
		\source{octave}{../es4/script8_1.m}
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Risolviamo il sistema lineare}
		\source{octave}{../es4/script8_2.m}
	\end{block}
\end{frame}
