\section{Integrazione numerica}

\begin{frame}{Integrazione numerica}
	\begin{block}{Troviamo un'approssimazione}
		\footnotesize
		Spesso calcolare l'integrale di una funzione reale $f$ su un intervallo
		$[a, b]$ risulta difficile perciò se ne cerca un'approssimazione
		sostituendo $f$ con un'altra funzione che la approssima o la interpola
		e che risulta di più facile integrazione.
	\end{block}
	\begin{block}{Formule interpolatorie}
		Tramite delle formule possiamo sostituire la funzione $f(x)$ con
		il suo polinomio interpolatore $P(x)$ tale che $P_n(x_i) = f(x_i)$
		per $i=0,\dots,n$ dove $x_i$ sono opportuni punti di discretizzazione.
		\begin{center}$I(f) \approx I(P_n) = \int_a^b P_n(x)\mathrm{d}x.$\end{center}
		Le formule che utilizziamo sono chiamate \textbf{formule di
		Newton-C\^otes} perché scegliamo i punti $x_i$ equidistanti in $[a, b]$.
	\end{block}
\end{frame}

%-------------SubSezione: Formule semplici di integrazione
\subsection{Formule semplici di integrazione}
\begin{frame}
	\begin{block}{Formule interpolatorie di Newton-C\^otes}
		\vspace{0.1cm}
		\begin{minipage}{\columnwidth}
		Dato l'intervallo $[a,b]$, fissato $n>0$, si determinano $n+1$ punti
		equidistanti mediante la relazione $x_i = a + i \cdot h$ con
		$h = \frac{b-a}{n},$ e $i=0,\dots,n.$ Si considera poi il polinomio
		interpolatore dei dati $(x_i, f(x_i))$ per $i=0,\dots,n$ espresso
		nella forma di Lagrange.
		\end{minipage}
	\end{block}
	\begin{block}{Formula dei trapezi}
		\footnotesize
		$n = 1$, $h = b - a$. Si hanno 2 soli punti di interpolazione
		$x_0 = a$ e $x_1 = b$. Il polinomio interpolatore è la retta passante
		per $(x_0, f(x_0))$ e $(x_1, f(x_1))$.
		\vspace{0.1cm}

		Termine d'errore: $-\frac{h^3}{12}\,f^{(2)}(\xi)$
	\end{block}
	\begin{block}{Formula di simpson}
		\footnotesize
		$n = 2$, $h = \frac{b - a}{2}$. Si hanno 3 punti di interpolazione
		$x_0 = a$, $x_1 = a + h$ e $x_2 = b$. Il polinomio interpolatore è quindi
		quello passante per $(x_0, f(x_0))$, $(x_1, f(x_1))$ e $(x_2, f(x_2))$.
		\vspace{0.1cm}
		
		Termine d'errore: $-\frac{h^5}{90}\,f^{(4)}(\xi)$
	\end{block}
\end{frame}

%-------------SubSezione: Formule composite di integrazione
\subsection{Formule composite di integrazione}
\begin{frame}
	\begin{block}{Formule composite}
		Potremmo pensare che aumentando il grado del polinomio interpolatore
		si ottengano formule più precise, però ciò non accade per via del
		fenomeno della cancellazione di cifre significative.
		\vspace{0.2cm}

		Introduciamo quindi le formule composite che suddividono l'intervallo
		$[a, b]$ in k sottointervalli e chiamano una formula d'integrazione
		di grado basso su ciascuno di essi.
		\vspace{0.2cm}

		Se suddividiamo l'intervallo iniziale in sottointervalli molto
		piccoli possono iniziare ad emergere degli errori numerici e quindi
		otterremmo dei risultati poco attendibili.
	\end{block}
	\begin{block}{Metodo basato sul criterio di Richardson}
		Il metodo consiste nell'applicare le formule di Newton-C\^otes composite
		suddividendo l'intervallo $[a, b]$ iterativamente fino a raggiungere
		la precisione prefissata.
	\end{block}
\end{frame}

\begin{frame}
	\riq{
		\inputminted[fontsize=\scriptsize,linenos=true,tabsize=4]{octave}{../es7/trapezi.m}
	}
	\riq{
		\inputminted[fontsize=\scriptsize,linenos=true,tabsize=4]{octave}{../es7/simpson.m}
	}
	\riq{
		\inputminted[fontsize=\scriptsize,linenos=true,tabsize=4]{octave}{../es7/trapezi_c.m}
	}
	\riq{
		\inputminted[fontsize=\scriptsize,linenos=true,tabsize=4]{octave}{../es7/simpson_c.m}
	}
\end{frame}

%-------------SubSezione: Esercizio 1
\subsection{Esercizio 1}
\begin{frame}
	\begin{exampleblock}{Esercizio 1}
		\footnotesize
		Risolvere numericamente i seguenti integrali:
		\begin{align*}
			\int_0^4 &2^x dx \qquad \int_1^2 \frac{1}{1+x} dx \qquad \int_0^1 \frac{1+x}{1+x^3} dx \qquad
			\int_1^2 \frac{1}{1+x^2} dx \\ &\int_2^3 3x^3+2x+2 dx \qquad \int_0^1 e^{x^2} dx \qquad
			\int_0^2 \sin(10x) dx
		\end{align*}
		Facendo uso:
		\begin{itemize}
			\item delle formule semplici di integrazioni dei Trapezi e di Simpson,
			\item delle formule composite di integrazioni dei Trapezi e di Simpson,
			implementando le stime di Richardson per la ricerca automatica del
			passo di suddivisione $h$, per cui si abbia una precisione di $10^{-5}$.
			\vspace{0.2cm}
			
			Per ognuno degli integrali proposti, confrontare il numero
			di suddivisioni dell’intervallo di integrazione richiesto dai
			due metodi per ottenere una stima dell’integrale secondo la
			precisione di richiesta.
		\end{itemize}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\scriptsize
	Errore relativo dei due metodi rispetto al valore reale degli integrali
	\begin{center}
		\tiny
		\begin{tabular}{|c|c|c|c|c|c|}
			\hline
			$\int$\# & Valore esatto & Trapezi & Errore trapezi &
			Simpson & Errore Simpson\\
			\hline
			\csvreader[late after line=\\ \hline]
			{../es7/data1.txt} % Il nome del file
			{a=\a,b=\b,c=\c,d=\d,e=\e,f=\f}
			{\texttt{\a} & \texttt{\b} & \texttt{\c} & \texttt{\d} & \texttt{\e} & \texttt{\f}}
		\end{tabular}
	\end{center}
	Iterazioni richieste dal metodo dei trapezi composito per raggiungere prec. $10^{-5}$
	\begin{center}
		\tiny
		\begin{tabular}{|c|c|c|c|c|c|}
			\hline
			$\int$\# & Valore esatto & Trapezi composito & N. Iterazioni &
			Errore relativo\\
			\hline
			\csvreader[late after line=\\ \hline]
			{../es7/data2.txt} % Il nome del file
			{a=\a,b=\b,c=\c,d=\d,e=\e,f=\f,g=\g,h=\h,i=\i}
			{\texttt{\a} & \texttt{\b} & \texttt{\c} & \texttt{\d} & \texttt{\e}}
		\end{tabular}
	\end{center}
	Iterazioni richieste dal metodo di Simpson composito per raggiungere prec. $10^{-5}$
	\begin{center}
		\tiny
		\begin{tabular}{|c|c|c|c|c|c|}
			\hline
			$\int$\# & Valore esatto & Simpson composito & N. Iterazioni &
			Errore Simpson\\
			\hline
			\csvreader[late after line=\\ \hline]
			{../es7/data2.txt} % Il nome del file
			{a=\a,b=\b,c=\c,d=\d,e=\e,f=\f,g=\g,h=\h,i=\i}
			{\texttt{\a} & \texttt{\b} & \texttt{\f} & \texttt{\g} & \texttt{\h}}
		\end{tabular}
	\end{center}
\end{frame}
