#!/bin/bash

pushd ..
for i in es*/; do
  pushd $i;
  if test -n "$(shopt -s nullglob; echo genera*.m)"
  then
    echo genera*.m
    for j in genera*.m; do
      octave --eval $(echo $j | cut -d'.' --complement -f2-);
    done
  fi
  popd;
done
popd;
