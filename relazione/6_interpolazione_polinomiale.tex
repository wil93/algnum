\section{Interpolazione polinomiale}

\begin{frame}{Interpolazione polinomiale}
	%~ \begin{block}{Rappresenatare dati sperimentali}
		Il problema dell'interpolazione di dati sperimentali nasce dall’esigenza di
		rappresentare in maniera \emph{continua} un fenomeno reale di cui abbiamo
		solo una valutazione \emph{discreta}.
	%~ \end{block}
	\begin{block}{Polinomio interpolatore}
		In generale note le coppie $(x_i, y_i)\quad i=0,\dots,n,\quad x_i \ne x_k\quad i \ne k$,
		dove i punti $y_i$ rappresentano le valutazioni del fenomeno nelle
		posizioni $x_i$, il prolema dell'interpolazione consiste nel trovare
		il polinomio di grado $n$
		$$P_n(x) = \alpha _0 + \alpha _1 x + \alpha _2 x^2 + \dots + \alpha _n x^n$$
		tale che
		$$P_n(x_i) = y_i, \qquad i = 0,\dots,n$$
		Tale polinomio prende il nome di polinomio interpolatore.
	\end{block}
\end{frame}


\begin{frame}
	\begin{block}{Unicità del polinomio interpolatore}
		\footnotesize
		Imporre $P_n(x_i) = y_i, \mathrm{per\ }i=0\dots,n$ significa risolvere
		il sistema lineare:
		$$
		\begin{cases}
			\alpha_0 + \alpha_1 x_0 + \alpha_2 x_0^2 + \dots + \alpha_n x_0^n = y_0 \\
			\dots \\
			\alpha_0 + \alpha_1 x_n + \alpha_2 x_n^2 + \dots + \alpha_n x_n^n = y_n
		\end{cases}
		$$
		La matrice dei coefficienti che otteniamo è quadrata perchè il numero
		delle condizioni che imponiamo è uguale al numero delle incognite, ed ha
		rango massimo perchè abbiamo assunto che $x_i \ne x_k \quad i \ne k$.
		
		Quindi il polinomio interpolatore \textit{esiste sempre} ed è \textit{unico}.
	\end{block} \pause
	\begin{alertblock}{Attenzione}
		Osservando la matrice dei coefficienti notiamo che è proprio la matrice di
		Vandermonde di ordine $n$. Sappiamo che tale matrice è molto sensibile
		alle perturbazioni sui dati. Occorre quindi prendere una strada
		differente per risolvere il problema.
	\end{alertblock}
\end{frame}

%-------------SubSezione: Forma di Lagrange
\subsection{Forma di Lagrange}
\begin{frame}
	\begin{block}{Polinomio interpolatore nella forma di Lagrange}
		\footnotesize
		Date le coppie $(x_i, y_i)\quad i=0,\dots,n,\quad x_i \ne x_k\quad i \ne k$
		il polinomio interpolatore può essere espresso nella seguente forma
		di Lagrange:
		$$P_n(x) = \sum_{j=0}^n y_j L_j(x)$$
		dove
		$$L_j(x) = \prod_{\substack{k=0\\ k \ne j}}^n \frac{(x - x_k)}{(x_j - x_k)}$$
		è il \textit{j-}esimo polinomio di Lagrange.
		\vspace{0.2cm}
		
		$L_j(x)$ è un polinomio che gode della seguente proprietà di cardinalità:
		$$
		L_j(x_k) = 
		\begin{cases}
			1 \quad se\quad k = j \\
			0 \quad se\quad k \ne j
		\end{cases}
		$$
	\end{block}
\end{frame}

\begin{frame}
	\riq{
		\source{octave}{../es6/lagrange.m}
	}
	\begin{block}{Complessità computazionale}
		L'algoritmo ha complessità $\ogrande(2n^2)$.
		
		Pertanto valutare un polinomio di Lagrange in $M$ punti costa $\ogrande(M\cdot 2n^2)$.
	\end{block}
\end{frame}

%-------------SubSezione: Forma di Newton
\subsection{Forma di Newton}
\begin{frame}
	\begin{block}{Polinomio interpolatore di Newton mediante differenze divise}
		La forma di Newton consente di calcolare il valore di $P_n(x)$ in
		un punto $x_0$ in un numero inferiore di operazioni rispetto alla
		forma di Lagrange.\\
		\vspace{0.2cm}
		Per rappresentare il polinomio di Newton è necessario prima introdurre
		il concetto di \textbf{differenze divise}.
	\end{block}
	\begin{block}{Differenze divise}
		Siano $\{ x_0, x_1, \dots, x_n\}$ $n+1$ punti assegnati
		e distinti nell'intervallo $\left[ a, b \right]$ e siano $y_i = f(x_i)$
		i valori assunti dalla funzione $f(x)$ definita in $\left[ a, b \right]$,
		si definisce in generale \textbf{differenza divisa} di ordine $n$ relativa ai
		punti $x_i, x_{i+1}, \dots, x_j$ per $i \le j$ la funzione:
		$$
		f\left[x_i, \dots, x_j\right] = \begin{cases}
			f(x_i) & \mbox{se } i = j\\
			\frac{f\left[x_i, \dots, x_{j-1}\right] - f\left[x_{i+1},
				\dots, x_j\right]}{x_i - x_j} & \mbox{altrimenti}
		\end{cases}
		$$
	\end{block}
\end{frame} 

\begin{frame}
	\riq{
		\source{octave}{../es6/differenze_div.m}
	}
	%~ \begin{block}{}
	\\\vspace{0.3cm}
		Al termine dell'algoritmo nel vettore $d$ sono conservati i valori
		$$d_k = f\left[ x_0, x_1, \dots, x_k\right], \quad k = 0, 1, \dots,n$$
	%~ \end{block}
	\begin{block}{Complessità computazionale}
		Il costo dell'algoritmo è $\ogrande\left(\frac{n^2}{2}\right)$
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Polinomio interpolatore nella forma di Newton}
		\footnotesize
		Una volta definite le differenze divise il polinomio interpolatore
		di Newton si esprime nella seguente forma:
		\begin{align*}
		P_n(x) &= f\left[x_0\right] + \left(x - x_0\right)f\left[x_0, x_1\right]
		+ \left(x - x_0\right)\left(x - x_1\right) f\left[x_0, x_1, x_2\right]
		+ \cdots \\ &+ \left(x - x_0\right)\left(x - x_1\right)\cdots
		\left(x - x_{n-1}\right) f\left[x_0, x_1,\dots,x_n\right]
		\end{align*}
		Che possiamo riscrivere seguendo lo \emph{schema di Horner}:
		$$
		P_n(x) = d_0 + (x - x_0)(d_1 + (x - x_1)(d_2 + \cdots (d_{n-1}
		+ (x - x_{n-1})d_n)\cdots)
		$$
	\end{block}
	\riq{
		\source{octave}{../es6/newton_interp.m}
	}
\end{frame}

\begin{frame}
	\begin{block}{Complessità computazionale}
		Per valutare il polinomio in un punto (utilizzando la forma di Horner)
		impieghiamo $\ogrande(n)$ operazioni a cui vanno sommate le operazioni
		effettuate per calcolare le differenze divise
		$\ogrande\left(\frac{n^2}{2}\right)$. Il costo totale è
		$\ogrande\left(\frac{n^2}{2}\right) + \ogrande(n)$, quindi valutare
		$M$ punti ha una complessità pari a $\ogrande\left(\frac{n^2}{2}\right)
		+ \ogrande(M\cdot n).$
	\end{block}
	\begin{block}{Confronto dei tempi di esecuzione}
		Ad esempio, per valutare il polinomio interpolatore di 15 punti
		equidistanti nell'intervallo $[-1, 1]$ in 1000 punti impieghiamo:
		\begin{itemize}
			\item 0.0248 secondi con il metodo di Lagrange
			\item 0.0035 secondi con il metodo di Newton
		\end{itemize}
	\end{block}
\end{frame}

%-------------SubSezione: Convergenza del polinomio interpolatore
\subsection{Convergenza del polinomio interpolatore}
\begin{frame}
	\begin{block}{Convergenza del polinomio interpolatore}
		Al crescere del numero di punti di interpolazione, e conseguentemente
		del grado del polinomio, con i punti $x_i$ scelti equidistanti
		nell'intervallo $[a, b]$, non si ha in genere la convergenza
		del polinomio interpolatore alla funzione che ha generato i dati.
		In particolare otteniamo una buona approssimazione al centro dell'intervallo
		e delle fitte oscillazioni agli estremi.
	\end{block}
	\begin{block}{Zeri dei polinomi di Chebyshev}
		Se i punti $x_i$ vengono scelti come zeri dei polinomi di Chebyshev
		$$
		x_i = \cos\left(\frac{2i-1}{2n} \pi\right) \quad i=1,\dots,n
		$$
		allora all'aumentare dei punti di interpolazione si ha la convergenza del
		polinomio alla funzione generatrice.
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 1
\subsection{Esercizio 1}
\begin{frame}
	\begin{exampleblock}{Esercizio 1}
		\footnotesize
		Note le coppie $(x_i, y_i)$ per $i=0,\dots,n$ dove i punti $x_i$ possono
		essere:
		\begin{enumerate}
			\item equidistanti nell'intervallo $[-1, 1]$
			\item zeri del polinomio di Chebyshev
		\end{enumerate}
		e i valori $y_i$ sono dati da $y_i = f(x_i)$, cioè il vettore
		ottenuto valutando nei punti $x_i$ le seguenti funzioni:
		\begin{enumerate}
			\item $f_1(x) = \frac{1}{1+25x^2}$ {\tiny $\qquad \leftarrow$ \emph{per motivi di
				spazio useremo solo $f_1$ in questa relazione}}
			\item $f_2(x) = |x|$
		\end{enumerate}
		Realizzare uno script Matlab che permetta di calcolare il polinomio 
		interpolatore di Lagrange ed il polinomio interpolatore di Newton e 
		valutarli al crescere di $n$, numero di osservazioni conosciute, in
		$m = 1000$ punti, scelti equidistanti dell'intervallo $[-1,1]$. 
		\vspace{0.2cm}
		
		Rappresentare in un grafico, il polinomio interpolatore, la funzione
		che ha generato i dati negli $m$ punti di valutazione e l'errore 
		commesso punto per punto come differenza in valore assoluto tra il 
		polinomio interpolatore e la funzione che ha generato i dati.
	\end{exampleblock}
\end{frame}

\begin{frame}[t]
	\begin{block}{}
		$n=\only<1,2,7,8>{5}\only<3,4,9,10>{10}\only<5,6,11,12>{15}$ punti
		\only<1-6>{\textbf{equidistanti} in $[-1,1]$}\only<7-12>{scelti come
		\textbf{zeri del polin. di Chebyshev}} valutati con $f_1$.
	\end{block}
	\only<1-1>{  \input{../es6/plot1_5.tex} }
	\only<2-2>{  \input{../es6/plot2_5.tex} }
	\only<3-3>{  \input{../es6/plot1_10.tex}}
	\only<4-4>{  \input{../es6/plot2_10.tex}}
	\only<5-5>{  \input{../es6/plot1_15.tex}}
	\only<6-6>{  \input{../es6/plot2_15.tex}}
	\only<7-7>{  \input{../es6/plot3_5.tex} }
	\only<8-8>{  \input{../es6/plot4_5.tex} }
	\only<9-9>{  \input{../es6/plot3_10.tex}}
	\only<10-10>{\input{../es6/plot4_10.tex}}
	\only<11-11>{\input{../es6/plot3_15.tex}}
	\only<12-12>{\input{../es6/plot4_15.tex}}
\end{frame}

%-------------SubSezione: Esercizio 2
\subsection{Esercizio 2}
\begin{frame}
	\begin{exampleblock}{Esercizio 2}
		Realizzare uno script Matlab che permetta all'utente di inserire in
		maniera interattiva facendo click sulla finestra grafica i punti di
		coordinate $(x_i, y_i)$ per $i=0,\dots,n$ dei punti $P_i$ che si
		vogliono interpolare. L'utente valuti se:
		\begin{enumerate}
		\item I punti $P_i$ sono tali da poter rappresentare punti
			appartenenti ad una funzione e quindi scelga di interpolarli
			mediante funzioni interpolanti.
		\item I punti $P_i$ sono tali da poter rappresentare punti
			appartenenti ad una curva e quindi scelga di interpolarli
			mediante curve interpolanti.
		\end{enumerate}
	\end{exampleblock}
\end{frame}
