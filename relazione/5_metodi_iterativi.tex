\section{Metodi iterativi per la soluzione di sistemi lineari}

\begin{frame}{Metodi iterativi per la soluzione di sistemi lineari}
	\begin{block}{Utili per matrici sparse}
		Sono metodi che raggiungono la soluzione esatta attraverso il
		calcolo del limite di un procedimento iterativo. Al contrario
		dei metodi diretti che fattorizzano la matrice e che hanno quindi
		complessità $\ogrande(n^3)$, i metodi iterativi producono una
		\textit{decomposizione} della matrice e hanno di conseguenza
		complessità $\ogrande(n^2)$ e sono adatti per matrici sparse di
		grandi dimensioni.
	\end{block}
	\begin{block}{Decomposizione}
		Sia dato il sistema $Ax=b$ con $\det A \ne 0$, consideriamo la
		decomposizione $A=M-N$ con $\det M \ne 0$. Il sistema diventa quindi
		$(M-N)x=b$ ovvero $Mx=Nx+b$ da cui $x=M^{-1}Nx+M^{-1}b$. La matrice
		$T\vcentcolon=M^{-1}N$ prende il nome di \emph{matrice di iterazione}.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{$x=M^{-1}Nx+M^{-1}b$}
		L'idea alla base dei metodi iterativi è quindi la seguente:
		scegliamo in modo arbitrario un vettore $x^{(0)}$ che rappresenterà
		la nostra prima ``stima'' della soluzione al sistema originario
		$Ax=b$. Costruiamo a quel punto una successione $x^{(k)}$ mediante
		il procedimento iterativo:
		$$x^{(k)}=M^{-1}Nx^{(k-1)}+M^{-1}b,\quad\mathrm{per\ }k=1,2,\dots$$
		Ora, \emph{se il procedimento iterativo è convergente}, si avrà:
		$$\lim_{k\to\infty} x^{(k)} = z$$
		Sostituendo in $Mx^{(k)}=Nx^{(k-1)}+b$ per $k\to\infty$ abbiamo:
		$$Mz=Nz+b\quad\Rightarrow\quad (M-N)z=b\quad\Rightarrow\quad Az=b$$
		quindi $z$ è soluzione del sistema originario.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Ma quando converge?}
		Condizione necessaria e sufficiente affinché un procedimento
		iterativo converga a $z$ per ogni scelta di $x^{(0)}$ è che il
		\emph{raggio spettrale} della matrice di iterazione $T$ (definito
		come il suo più grande autovalore in modulo) sia strettamente
		inferiore di 1. Vale a dire: $$\lim_{k \to \infty} x^{(k)} = x,
		\forall x^{(0)} \Leftrightarrow \rho(T) < 1$$
	\end{block}
	\begin{block}{In pratica...}
		Implementeremo due metodi simili tra di loro che si basano su una
		scomposizione di $A$ nella somma di tre matrici $D+E+F$ ottenute
		azzerando alcuni elementi da $A$. Rispettivamente, $D, E$ ed $F$
		conservano gli elementi sulla diagonale, sotto la diagonale e
		sopra la diagonale.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Metodo di Jacobi}
		\footnotesize
		Anche detto metodo degli spostamenti simultanei, consiste nello
		scegliere le matrici $M$ e $N$ rispettivamente uguali a $D$ e a
		$-(E+F)$. La matrice di iterazione sarà quindi:
		$T_J=M^{-1}N=-D^{-1}(E+F)=I-D^{-1}A$.\\
		Il procedimento iterativo sarà:
		\begin{center}$x^{(k)}=-D^{-1}(E+F)x^{(k-1)}+D^{-1}b$\end{center}
	\end{block}
	\begin{block}{Metodo di Gauss-Seidel}
		\footnotesize
		Anche detto metodo degli spostamenti successivi, consiste nello
		scegliere le matrici $M$ e $N$ rispettivamente uguali a $E+D$ e
		a $-F$. La matrice di iterazione sarà quindi:
		$T_G=M^{-1}N=-(E+D)^{-1}F$.\\
		Il procedimento iterativo sarà:
		\vspace{0.2cm}
		
		\begin{center}$x^{(k)}=-D^{-1}(Ex^{(k)}+Fx^{(k-1)}-b)$\end{center}
		
		Il metodo può anche essere accelerato tramite un parametro di
		rilassamento.
	\end{block}
\end{frame}

\begin{frame}
	Metodo di Jacobi
	\riq{
		\source{octave}{../es5/jacobi.m}
	}
\end{frame}

\begin{frame}
	Metodo di Gauss-Seidel
	\riq{
		\source{octave}{../es5/gauss_seidel.m}
	}
\end{frame}

\begin{frame}
	Metodo di Gauss-Seidel con rilassamento
	\riq{
		\source{octave}{../es5/gauss_seidel_SOR.m}
	}
\end{frame}

\begin{frame}
	\begin{exampleblock}{Esercizio 1}
		Costruire uno script Matlab che presi in input:
		\begin{itemize}
 			\item la matrice $A$ di ordine $n$,
			\item il termine noto $b$ scelto in maniera tale che la soluzione 
			del sistema $Ax=b$ sia il vettore unitario, 
			\item il vettore soluzione iniziale \texttt{x0 = zeros(n,1)},
			\item il numero massimo di iterazioni \texttt{maxiters = 3000},
			\item la precisione \texttt{prec = 1e-5},
		\end{itemize}
		risolva se possibile, usando i tre metodi visti, i seguenti sistemi
		lineari e grafichi l'errore relativo ad ogni iterazione.
		Confrontare il raggio spettrale e il numero di iterazioni date in
		output dall'esecuzione di ciascun metodo.
		\begin{itemize}
			\item Sperimentare i metodi di Jacobi e Gauss-Seidel sulle matrici $A$ e
			$B$.
			\item Sperimentare i metodi di Jacobi, Gauss-Seidel e Gauss-Seidel
			con rilassamento sulle matrici $C$, $D$ ed $E$.
		\end{itemize}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\only<1-1>{
		\begin{exampleblock}{Matrice A}
			Sperimentare i metodi di Jacobi e Gauss-Seidel sulla matrice:
			\Huge{
				$$
				A = \left[ \begin{array}{ccc}
					3 & 0 & 4 \\
					7 & 4 & 3\\
					-1 & -1 & -2\end{array} \right]
				$$
			}
		\end{exampleblock}
	}
	\only<2-2>{
		\begin{block}{Matrice $A$}
			Utilizzando il metodo di Jacobi il sistema associato alla matrice $A$
			non converge, infatti il raggio spettrale della matrice d'iterazione è
			$\rho \approx 1.37$.
			Il metodo di Gauss-Seidel riesce invece a risolvere il sistema, con raggio
			spettrale $\rho = 0.125$. Di seguito, il grafico dell'errore in funzione
			del numero di iterazioni $k$.
		\end{block}
		\input{../es5/plot1.tex}
	}
\end{frame}

\begin{frame}
	\only<1-1>{
		\begin{exampleblock}{Matrice B}
			Sperimentare i metodi di Jacobi e Gauss-Seidel sulla matrice:
			\huge{
				$$
				B = \left[ \begin{array}{cccc}
				5 & 0 & -1 & 2\\
				-2 & 4 & 1 & 0\\
				0 & -1 & 4 & -1\\
				2 & 0 & 0 & 3\end{array} \right]
				$$
			}
		\end{exampleblock}
	}
	\only<2-2>{
		\begin{block}{Matrice $B$}
			Contrariamente a quanto accaduto con la matrice $A$, entrambi i metodi
			riescono ad approssimare il sistema associato alla matrice $B$. Il
			raggio spettrale di Jacobi è $\rho \approx 0.53$ mentre quello di Gauss è
			$\rho \approx 0.13$.
			Di seguito il grafico dell'errore in funzione del numero di iterazioni $k$.
		\end{block}
		\input{../es5/plot2.tex}
	}
\end{frame}

\begin{frame}[t]
	\only<1-1>{
		\begin{exampleblock}{Matrice C}
			Sperimentare i metodi di Jacobi e Gauss-Seidel e Gauss-Seidel con
			rilassamento sulla matrice:\\
			\Large{
				$$
				C = \left[ \begin{array}{cccccc}
				3 & -1 & 0 & 0 & 0 & -1\\
				-1 & 3 & -1 & 0 & -1 & 0\\
				0 & -1 & 3 & -1 & 0 & 0\\ 
				0 & 0 & -1 & 3 & -1 & 0\\
				0 & -1 & 0 & -1 & 3 & -1\\
				-1 & 0 & 0 & 0 & -1 & 3\end{array} \right]
				$$
			}
		\end{exampleblock}
	}
	\only<2-4>{
		\begin{block}{Matrice $C$}
			I metodi di Jacobi e di Gauss riportano raggio spettrale rispettivamente
			$\rho \approx 0.80$ e $\rho \approx 0.66$, quindi entrambi sono
			un po' lenti. Utilizzando Gauss-Seidel riusciamo ad ottenere una sostanziale
			velocizzazione ottenendo $\rho \approx 0.41$ in corrispondenza
			di $\omega^*$.
		\end{block}
	}
	\only<2-2>{
		\input{../es5/plot3.tex}
	}
	\only<3-3>{
		\input{../es5/plot4.tex}
	}
	\only<4-4>{
		\input{../es5/plot5.tex}
	}
\end{frame}

\begin{frame}[t]
	\only<1-1>{
		\begin{exampleblock}{Matrice D}
			Sperimentare i metodi di Jacobi, Gauss-Seidel e Gauss-Seidel con
			rilassamento sulla matrice:\\
			$D$ = \textit{matrice tridiagonale di ordine 10, con elementi
			diagonali uguali a 4, elementi della codiagonale superiore
			uguali a -1, elementi della codiagonale inferiore uguali a -1;}
		\end{exampleblock}
	}
	\only<2-4>{
		\begin{block}{Matrice $D$}
			I metodi di Jacobi e di Gauss riportano raggio spettrale rispettivamente
			$\rho \approx 0.48$ e $\rho \approx 0.23$. Utilizzando Gauss-Seidel
			riusciamo a velocizzazione di parecchio i metodi precedenti ottenendo
			$\rho = 0.066$ in corrispondenza di $\omega^*$.
		\end{block}
	}
	\only<2-2>{
		\input{../es5/plot6.tex}
	}
	\only<3-3>{
		\input{../es5/plot7.tex}
	}
	\only<4-4>{
		\input{../es5/plot8.tex}
	}
\end{frame}

\begin{frame}[t]
	\only<1-1>{
		\begin{exampleblock}{Matrice E}
			Sperimentare i metodi di Jacobi, Gauss-Seidel e Gauss-Seidel con
			rilassamento sulla matrice:\\
			$E$ = \textit{matrice tridiagonale di ordine 10, con elementi
				diagonali uguali a 2, elementi della codiagonale superiore
				uguali a -1, elementi della codiagonale inferiore uguali a -1.}
		\end{exampleblock}
	}
	\only<2-4>{
		\begin{block}{Matrice $E$}
			I metodi di Jacobi e di Gauss riportano raggio spettrale rispettivamente
			$\rho \approx 0.96$ e $\rho \approx 0.92$, sono quindi molto lenti.
			Utilizzando Gauss-Seidel riusciamo a velocizzazione di parecchio
			i metodi precedenti ottenendo $\rho = 0.562$ in corrispondenza di
			$\omega^*$.
		\end{block}
	}
	\only<2-2>{
		\input{../es5/plot9.tex}
	}
	\only<3-3>{
		\input{../es5/plot10.tex}
	}
	\only<4-4>{
		\input{../es5/plot11.tex}
	}
\end{frame}
