\section{Calcolo degli zeri di funzioni}

\begin{frame}{Calcolo degli zeri di funzioni}
	\begin{block}{Teorema degli zeri, o di Bolzano}
		Il \textit{teorema degli zeri} per le funzioni continue afferma
		che, data una funzione $f(x)$ continua su un intervallo chiuso e
		limitato $[a,b]$ tale che $f(a)f(b) < 0$, siamo sicuri che
		$\exists c \in \left]a,b\right[ : f(c) = 0$.
	\end{block}

	\begin{block}{Nella pratica però}
		\footnotesize
		L'approssimazione numerica degli zeri di una funzione deve tener
		conto di diversi aspetti tra cui:
		\begin{itemize}
			\item \textit{convergenza},
			\item \textit{velocità di convergenza},
			\item scelta di un \textit{intervallo di convergenza},
			\item \textit{criteri d'arresto},
			\item \textit{buona o cattiva posizione del problema}.
		\end{itemize}
	\end{block}
\end{frame}

\subsection{Metodo di bisezione}
\begin{frame}
	\begin{block}{Metodo di bisezione}
		Il metodo di bisezione si basa sul teorema di Bolzano, infatti
		consiste nel ``circondare'' lo zero della funzione in intervalli
		sempre più piccoli fino al suo raggiungimento.
		Il metodo è a convergenza globale e converge linearmente con $p=1$
		e $c=\frac12$.
	\end{block}
	\riq{
		\source{octave}{../es2/bisezione.m}
	}
\end{frame}

\begin{frame}
	Alcuni passi del metodo di bisezione:\\
	\vspace{0.1cm}
	\begin{center}
		\includegraphics[width=6.5cm]{../es2/bisez.png}
	\end{center}
\end{frame}

\subsection{Metodo della regula falsi}
\begin{frame}
	\begin{block}{Metodo della regula falsi}
		Il metodo della regula falsi fissa un punto iniziale e considera le
		rette passanti per tale punto. Ad ogni iterazione ne cerca una che si
		avvicina di più al punto cercato, modificando il coefficiente angolare.
		Anche questo metodo è a convergenza globale e converge linearmente.
	\end{block}
	\vspace{0.2cm}
	\riq{
		\source{octave}{../es2/regula_falsi.m}
	}
\end{frame}

\begin{frame}
	Alcuni passi del metodo della regula falsi:\\
	\vspace{0.1cm}
	\begin{center}
		\includegraphics[width=8cm]{../es2/regul.png}
	\end{center}
\end{frame}

\subsection{Metodo di Newton}
\begin{frame}
	\begin{block}{Metodo di Newton}
		Nel metodo di Newton, ad ogni passo $k$ si considera la retta
		passante per il	punto $(x_k, F(x_k))$ tangente la funzione
		e si determina il nuovo iterato come il punto di incontro tra questa
		retta e l'asse delle $x$.
		Questo metodo converge solo localmente, quindi è necessario scegliere
		un punto di partenza sufficientemente vicino alla soluzione.
		D'altro canto la convergenza è di ordine 2 (quadratica), quindi
		molto rapida.
	\end{block}
	\vspace{0.2cm}
	\riq{
		\source{octave}{../es2/newton.m}
	}
\end{frame}

\begin{frame}
	Alcuni passi del metodo di Newton:\\
	\vspace{0.1cm}
	\begin{center}
		\includegraphics[width=8cm]{../es2/newt.png}
	\end{center}
\end{frame}

\subsection{Metodo delle secanti}
\begin{frame}
	\begin{block}{Metodo delle secanti}
		Il metodo delle secanti è una variante del metodo di Newton che sostituisce
		alla tangente la retta passante per i punti $(x_k, F(x_k))$ e
		$(x_{k-1}, F(x_{k-i}))$.
		Per il metodo delle secanti l'ordine di convergenza è $p=\phi\approx
		1.618$, e anch'esso come il metodo di Newton converge solo localmente.
		Rispetto a quest'ultimo ha il vantaggio di non richiedere la
		derivata della funzione in esame, però richiede che venga dato un
		punto iniziale aggiuntivo.
	\end{block}
	\vspace{0.2cm}
	\riq{
		\source{octave}{../es2/secanti.m}
	}
\end{frame}

\begin{frame}
	Alcuni passi del metodo delle secanti:\\
	\vspace{0.1cm}
	\begin{center}
		\includegraphics[width=8.2cm]{../es2/secant.png}
	\end{center}
\end{frame}

%-------------SubSezione: Esercizio 1
\subsection{Esercizio 1}

\begin{frame}
	\begin{exampleblock}{Esercizio 1}
		Per saldare il valore di un acquisto di
		20\,000 euro sono necessarie 5 rate annuali di 4800 euro. Qual è
		l'interesse applicato? Ricordando la relazione matematica che lega
		il valore attuale $P$, il pagamento annuale $A$, il numero di anni
		$n$, l'interesse $i$, ricavare l'interesse che soddisfa questa
		relazione, con i metodi visti a lezione.
		$$A = p \frac{i(1+i)^n}{(1+i)^n - 1}$$
	\end{exampleblock}
 \end{frame}

\begin{frame}
	\begin{block}{Soluzione}
		Abbiamo definito una funzione dell'interesse $i$ come la differenza tra i due
		membri dell'espressione che relaziona $A$, $p$, $n$, $i$, ovvero:
		\begin{align*}
		f(i) &= A - p \frac{i(1+i)^n}{(1+i)^n - 1} \\
				 &= 4800 - 20\,000 \frac{i(1+i)^5}{(1+i)^5 - 1}
		\end{align*}
		Ora, quando questa differenza vale zero, vuol dire che i due membri
		dell'espressione iniziale sono uguali; quindi ci interessa semplicemente
		trovare uno zero della funzione $f(i)$.
	\end{block}
	\vspace{0.2cm}
	\riq{
		\source{octave}{../es2/script1.m}
	}\\
	\vspace{0.2cm}
	L'interesse cercato, \texttt{y(end)}, vale \texttt{0.0640224076431011}.
\end{frame}

%-------------SubSezione: Esercizio 2
\subsection{Esercizio 2}
\begin{frame}
	\begin{exampleblock}{Esercizio 2}
	\begin{minipage}{\textwidth}
		\footnotesize
		In una città gli abitanti del centro
		tendono a spostarsi nelle zone suburbane. È necessario studiare
		questo fenomeno per organizzare opportunamente le risorse. Sapendo
		che la popolazione urbana decresce secondo la seguente legge:
		$$P_{u}(t) = P_{u,max} e^{-k_{u} t} + P_{u, min}$$ e la popolazione
		suburbana cresce secondo la seguente legge:
		\begin{align*}
		&\quad \qquad \qquad \qquad
		P_{s}(t) = \frac{P_{s,max}}{1 + \frac{P_{s, max}}{P_{0} - 1} e^{-k_{s} t}}\\
		&P_{u, max} = 120\,000,\quad P_{u, min} = 60\,000,\quad k_{u} = 0.04,\quad
		k_{s} = 0.06, \\ &P_{s, max} = 300\,000,\quad P_{0} = 5000
		\end{align*}
		Determinare, con i metodi visti a lezione, il tempo $\overline{t}$,
		in cui le due popolazioni sono uguali, ed i valori $P_{s}(\overline{t})$
		e $P_{u}(\overline{t})$.
	\end{minipage}
	\end{exampleblock}
\end{frame}

\begin{frame}
	\begin{block}{Soluzione}
		Anche in questo caso abbiamo definito una nuova funzione $f(t)$, stavolta
		però come la differenza tra la popolazione urbana e la popolazione
		suburbana al tempo $t$:
		\begin{align*}
		f(t) &= P_{u}(t) - P_{s}(t) \\
				&= P_{u,max} e^{-k_{u} t} + P_{u, min} - \frac{P_{s,max}}{1 + \frac{P_{s, max}}{P_{0} - 1} e^{-k_{s} t}} \\
				&= 120\,000 e^{-0.04 t} + 60\,000 - \frac{300\,000}{1 + \frac{300\,000}{4999} e^{-0.06 t}}
		\end{align*}
	\end{block}
	\vspace{0.2cm}
	\riq{
		\source{octave}{../es2/script2.m}
	}\\
	\vspace{0.2cm}
	L'istante di tempo cercato, \texttt{y(end)}, vale \texttt{50.250241462354616}.
\end{frame}

%-------------SubSezione: Esercizio 3
\subsection{Esercizio 3}
\begin{frame}
	\begin{exampleblock}{Esercizio 3}
		Calcolare gli zeri di ciascuna delle seguenti funzioni applicando
		ciascuno dei metodi visti a lezione (bisezione, regula falsi, newton
		e secanti), con un errore relativo minore o uguale a $10^{-5}$:\\[0.2cm]
		\begin{tabular}{ p{5cm} l }
			$f(x)=x^2-2x-\log x$ & nell'intervallo $[1,4]$ \\
			$f(x)=\log\frac{2}{3-x}$ & nell'intervallo $[-1,2]$ \\
		\end{tabular}
	\end{exampleblock}
	\vspace{0.3cm}
	\begin{block}{Velocità di convergenza}
		Come ci si può aspettare, tutti i metodi visti a lezione prima o poi
		convergono agli zeri delle succitate funzioni, ma la velocità con la
		quale questi raggiungono la precisione desiderata varia significativamente.
	\end{block}
\end{frame}

\begin{frame}
	\begin{block}{Velocità di convergenza per la prima funzione}
		Le iterazioni necessarie sono state:
		\begin{itemize}
			\item \textit{19} per il metodo di bisezione.
			\item \textit{18} per il metodo della regula falsi.
			\item \textit{11} per il metodo delle secanti.
			\item \textit{6 } per il metodo di Newton.
		\end{itemize}
	\end{block}

	\begin{block}{Velocità di convergenza per la seconda funzione}
		Le iterazioni necessarie sono state:
		\begin{itemize}
			\item \textit{19} per il metodo di bisezione.
			\item \textit{17} per il metodo della regula falsi.
			\item \textit{8 } per il metodo delle secanti.
			\item \textit{6 } per il metodo di Newton.
		\end{itemize}
	\end{block}
\end{frame}

%-------------SubSezione: Esercizio 4
\subsection{Esercizio 4}
\begin{frame}
	\begin{exampleblock}{Esercizio 4}
		Determinare, con ciascuno dei metodi visti a lezione, tutte le
		radici della funzione $f(x)=x^3-1.9x^2-1.2x+2.5$ nell'intervallo
		$[-2,3]$, facendo uso del grafico per individuare i diversi intervalli di
		studio.
	\end{exampleblock}

	\input{../es2/plot8.tex}
\end{frame}

\begin{frame}
	\begin{block}{}
		Osservando il grafico con la funzione possiamo facilmente identificare
		i tre zeri della funzione e trovare tre intervalli che li racchiudono,
		ad esempio:
		$$[-1.5,-0.5], \qquad [0.5,1.5], \qquad [1.5,2.5]$$
	\end{block}
	%~ \begin{block}{}
	Ora, chiamando distintamente sui tre intervalli uno dei metodi visti, 
	possiamo trovare il valore degli zeri cercati:\\
	%~ \end{block}
	\vspace{0.2cm}
	\riq{
		\source{octave}{../es2/script4_1.m}
	}
\end{frame}
