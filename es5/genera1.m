close all;
clear all;
clc;

A = [3 0 4; 7 4 3; -1 -1 -2];
n = length(A);
b = sum(A')';
x0 = zeros(n, 1);
maxiters = 3000;
prec = 1e-5;
fprintf('\nMETODO DI GAUSS SEIDEL\n\n');
[x, err, k, rhoo] = gauss_seidel(A, b, x0, maxiters, prec);
figure;
plot(1:k, err, 'linewidth', 3);
xlabel('Iterazioni');
ylabel('Errore');
legend('Gauss-Seidel');
print('-dtex', 'plot1', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot1.tex');
