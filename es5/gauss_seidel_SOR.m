function [x, err, k, r] = gauss_seidel_SOR(A,b,x0,omega,maxiters,prec)
	x = x0;
	k = 0;
	err = zeros(1);
	D = diag(diag(A));
	M = D + omega * tril(A, -1);
	N = (1 - omega) * D - omega * triu(A, 1);
	T = M \ N;                                   % T = inv(M)*N
	r = rho(T);
	if r >= 1
		fprintf('Il metodo non converge\n');
		return;
	end
	do
		xpre = x;
		x = T * x + M \ b;                       % x = T*x + inv(M)*b
		err(++k) = norma(x - xpre, 0) / norma(x, 1);
	until k == maxiters || err(k) <= prec
end
