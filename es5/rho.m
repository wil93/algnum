function [r] = rho(A)
	%r = abs(eigs(A, 1, 'lm'));
	r = max(abs(eig(A)));
end
