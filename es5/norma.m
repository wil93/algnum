function [y] = norma(A, p)
	if p
		y = sum(A);
	else
		y = max(A);
	end
end
