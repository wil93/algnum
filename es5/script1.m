close all;
clear all;
clc;

% Scelta della matrice 
scelta = input('Scegli la matrice (1, 2, 3, 4, 5): ');

switch scelta
    case 1
        A = [3 0 4; 7 4 3; -1 -1 -2];
    case 2
        A = [5 0 -1 2; -2 4 1 0; 0 -1 4 -1; 2 0 0 3];
    case 3
        %A = [3 -1 0 0 0 -1; -1 3 -1 0 -1 0; 0 -1 3 -1 0 0; 0 0 -1 3 -1 0; 0 -1 0 -1 3 -1; -1 0 0 0 -1 3];
        nn = 6;
        A = diag(3 * ones(nn, 1)) + diag(-ones(nn-1, 1), -1) + diag(-ones(nn-1, 1), 1) + fliplr(diag(-ones(nn, 1)));
        A(nn/2, nn/2+1) ++;
        A(nn/2+1, nn/2) ++;
    case 4
        A = diag(4 * ones(10, 1)) + diag(-1 * ones(9, 1), -1) + diag(-1 * ones(9, 1), 1);
    case 5
        A = diag(2 * ones(10, 1)) + diag(-1 * ones(9, 1), -1) + diag(-1 * ones(9, 1), 1);
end

n = length(A);
% Facciamo in modo che la soluzione sia il vettore unitario
b = sum(A')';
x0 = zeros(n, 1);
maxiters = 3000;
prec = 1e-5;

% Metodo di Jacobi
fprintf('\nMETODO DI JACOBI\n\n');
[x, err, k, rhoo] = jacobi(A, b, x0, maxiters, prec);
fprintf('\n%.10f', rhoo);

%~ figure;
% Grafico con scala logaritmica
%~ semilogx(1:k, err);

% Metodo di Gauss Seidel
fprintf('\nMETODO DI GAUSS SEIDEL\n\n');
[x, err, k, rhoo] = gauss_seidel(A, b, x0, maxiters, prec);
fprintf('\n%.10f', rhoo);

figure;
xlabel('Iterazione $k$');
ylabel('Errore in funzione di $k$');
plot(1:k, err, 'linewidth', 3);
print('-dtex', 'plot1', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot1.tex');

% Il metodo di Gauss Seidel con rilassamento deve essere provato solo con
% le matrici 3, 4 e 5.
if ( scelta > 2)
   % Pre-allochiamo omega
   omega = zeros(501, 1);

   % Pre-allochiamo rhoo
   rhoo = zeros(501, 1);

   % Pre-allochiamo k
   k = zeros(501, 1);

   i = 1;
   for j = 0:0.0001:0.5
       % impostiamo il valore di omega
       omega(i) = 1 + 2 * j;
       
       % Metodo di Gauss Seidel con rilassamento
       [~, ~, k(i), rhoo(i)] = gauss_seidel_SOR(A, b, x0, omega(i), maxiters, prec);
       
       
       i = i + 1;
   end
   [~, idx] = min(rhoo);
   fprintf('\n%.10f', omega(idx));
   
   % Valore del raggio spettrale al variare di omega
   figure;
   xlabel('Iterazione $k$');
    ylabel('Errore in funzione di $k$');
   semilogx(omega, rhoo, 'linewidth', 3);
    print('-dtex', 'plot2', '-S700,300');
    system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot2.tex');
   
   % Numero di iterazioni richieste al variare di omega
   figure;
   xlabel('Iterazione $k$');
    ylabel('Errore in funzione di $k$');
   semilogx(omega, k, 'linewidth', 3);
    print('-dtex', 'plot3', '-S700,300');
    system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot3.tex');
end
