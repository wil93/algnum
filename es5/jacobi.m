function [x, err, k, r] = jacobi(A, b, x0, maxiters, prec)
	x = x0;
	k = 0;
	err = zeros(1);
	M = diag(diag(A));                           % M = D
	N = -(tril(A, -1) + triu(A, 1));             % N = -(E+F)
	T = M \ N;                                   % T = inv(M)*N
	r = rho(T);
	if r >= 1
		fprintf('Il metodo non converge\n');
		return;
	end
	do
		xpre = x;
		x = T * x + M \ b;                       % x = T*x + inv(M)*b
		err(++k) = norma(x - xpre, 0) / norma(x, 1);
	until k == maxiters || err(k) <= prec
end
