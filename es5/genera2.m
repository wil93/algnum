close all;
clear all;
clc;

A = [5 0 -1 2; -2 4 1 0; 0 -1 4 -1; 2 0 0 3];
n = length(A);
b = sum(A')';
x0 = zeros(n, 1);
maxiters = 3000;
prec = 1e-5;

fprintf('\nMETODO DI JACOBI\n\n');
[x, err, k, rhoo] = jacobi(A, b, x0, maxiters, prec);
fprintf('\nMETODO DI GAUSS SEIDEL\n\n');
[x, err2, k2, rhoo] = gauss_seidel(A, b, x0, maxiters, prec);

if (k < k2)
    for i=k+1:k2
        err(i)=0;
    end
    k=k2;
end

if (k2 < k)
    for i=k2+1:k
        err2(i)=0;
    end
    k2=k;
end

figure;
axis([1, 15, 0, 0.4]);
set(gca, 'XTick', [1:5:16]);
hold on;
plot(1:k,  err,  'b', 'linewidth', 3);
plot(1:k2, err2, 'r', 'linewidth', 3);
xlabel('Iterazioni');
ylabel('Errore');
legend('Jacobi', 'Gauss-Seidel');
print('-dtex', 'plot2', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot2.tex');
