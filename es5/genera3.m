close all;
clear all;
clc;

A = [3 -1 0 0 0 -1; -1 3 -1 0 -1 0; 0 -1 3 -1 0 0; 0 0 -1 3 -1 0; 0 -1 0 -1 3 -1; -1 0 0 0 -1 3];

n = length(A);
b = sum(A')';
x0 = zeros(n, 1);
maxiters = 3000;
prec = 1e-5;


fprintf('\nMETODO DI JACOBI\n\n');
[x, err, k, rhoo] = jacobi(A, b, x0, maxiters, prec);
fprintf('\nMETODO DI GAUSS SEIDEL\n\n');
[x, err2, k2, rhoo] = gauss_seidel(A, b, x0, maxiters, prec);

%~ if (k < k2)
    %~ for i=k+1:k2
        %~ err(i)=0;
    %~ end
    %~ k=k2;
%~ end
%~ 
%~ if (k2 < k)
    %~ for i=k2+1:k
        %~ err2(i)=0;
    %~ end
    %~ k2=k;
%~ end

figure;
axis([1, 20, 0, 0.26]);
hold on;
plot(1:k,  err,  'linewidth', 3);
plot(1:k2, err2, 'r', 'linewidth', 3);
xlabel('Iterazioni');
ylabel('Errore');
legend('Jacobi', 'Gauss-Seidel');
print('-dtex', 'plot3', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot3.tex');

omega = zeros(501, 1);
rhoo = zeros(501, 1);
k = zeros(501, 1);
i = 1;
for j = 0:0.001:0.5
    omega(i) = 1 + 2 * j;
    [~, ~, k(i), rhoo(i)] = gauss_seidel_SOR(A, b, x0, omega(i), maxiters, prec);
    i = i + 1;
end

omega_ott = 1.264; % OMEGA OTTIMALE

figure;
hold on;
plot(omega, rhoo, 'linewidth', 3);
plot([omega_ott, omega_ott], [0.4, 1.1], ':k', 'linewidth', 2, 'linestyle', '--');
xlabel('$\omega$');
ylabel('$\rho$');
legend('', '$\omega^*$');
print('-dtex', 'plot4', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot4.tex');
   
figure;
hold on;
plot(omega, k, 'linewidth', 3);
xlabel('$\omega$');
ylabel('Iterazioni');
print('-dtex', 'plot5', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot5.tex');
