close all;
clear all;
clc;

A = diag(4 * ones(10, 1)) + diag(-1 * ones(9, 1), -1) + diag(-1 * ones(9, 1), 1);

n = length(A);
b = sum(A')';
x0 = zeros(n, 1);
maxiters = 3000;
prec = 1e-5;


fprintf('\nMETODO DI JACOBI\n\n');
[x, err, k, rhoo] = jacobi(A, b, x0, maxiters, prec);
fprintf('\nMETODO DI GAUSS SEIDEL\n\n');
[x, err2, k2, rhoo] = gauss_seidel(A, b, x0, maxiters, prec);

if (k < k2)
    for i=k+1:k2
        err(i)=0;
    end
    k=k2;
end

if (k2 < k)
    for i=k2+1:k
        err2(i)=0;
    end
    k2=k;
end

figure;
axis([1, 10, 0, 0.14]);
hold on;
plot(1:k,  err,  'linewidth', 3);
plot(1:k2, err2, 'r', 'linewidth', 3);
xlabel('Iterazioni');
ylabel('Errore');
legend('Jacobi', 'Gauss-Seidel');
print('-dtex', 'plot6', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot6.tex');

omega = zeros(501, 1);
rhoo = zeros(501, 1);
k = zeros(501, 1);
i = 1;
for j = 0:0.001:0.5
    omega(i) = 1 + 2 * j;
    [~, ~, k(i), rhoo(i)] = gauss_seidel_SOR(A, b, x0, omega(i), maxiters, prec);
    i = i + 1;
end

omega_ott = 1.066; % OMEGA OTTIMALE

figure;
hold on;
plot(omega, rhoo, 'linewidth', 3);
plot([omega_ott, omega_ott], [0, 1.4], ':k', 'linewidth', 2, 'linestyle', '--');
xlabel('$\omega$');
ylabel('$\rho$');
legend('', '$\omega^*$');
print('-dtex', 'plot7', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot7.tex');
   
figure;
hold on;
plot(omega, k, 'linewidth', 3);
xlabel('$\omega$');
ylabel('Iterazioni');
print('-dtex', 'plot8', '-S700,300');
system('sed s/"plot[0-9]\+"/"..\/es5\/&"/g -i plot8.tex');
