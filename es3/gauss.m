function [L, A, P] = gauss(A)
	n = length(A);
	P = eye(n);
	L = zeros(n);
	for j = 1 : n-1
		[~, idx] = max(abs(A(j:end, j)));
		idx += j - 1;
		P([j idx], :) = P([idx j], :);
		A([j idx], :) = A([idx j], :);
		L([j idx], :) = L([idx j], :);
		L(j+1:end, j)  = A(j+1:end, j) / A(j, j);
		A(j+1:end, :) -= L(j+1:end, j) * A(j, :);
	end
	L += eye(n);
end
