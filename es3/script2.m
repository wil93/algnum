clear all;
close all;
clc;

for n = 2 : 15
    % Creazione matrice di Hilbert
    A = hilb(n);

    % Creazione vettore dei termini noti 
    b = sum(A')';
    
    % Calcolo del vettore con le soluzioni
    x = risolvi_sistema(A, b);
    
    % Calcolo errore relativo
    er = norma(ones(n, 1) - x, 1) / norma(ones(n, 1), 1);
    ic = ind_cond(A, 1);
    
    fprintf('%d\t%e\t%e\n', n, er, ic);
end
