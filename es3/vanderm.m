function [A] = vanderm(n, a, b)
	A = ones(n, n);
	x = linspace(a, b, n);
    for i = 2 : n
		A(:, i) = x.^(i-1)';
    end
end
