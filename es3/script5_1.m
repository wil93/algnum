>>> A = [10 1 4 0; 1 10 5 -1; 4 5 10 7; 0 -1 7 9];
>>> b = [15; 15; 26; 15];
>>> ic = ind_cond(A, 1)        % Calcolo indice condizionamento
ic =  62607.9999999880         % utilizzando la norma 1

>>> x = risolvi_sistema(A, b);
>>> bp = b;
>>> bp(1) *= 1.01;             % Effettuiamo la perturbazione
>>> xp = risolvi_sistema(A, bp);
>>> errs = norma(x - xp, 1) / norma(x, 1)  % Errore relativo
errs =  0.00171347138869398                % sulla soluzione

>>> errb = norma(b - bp, 1) / norma(b, 1)  % Errore relativo
errb =  0.00211267605633803                % sul termine noto
