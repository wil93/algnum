clear all
close all
clc

% Creazione matrice A
A = hilb(10) + eye(10);

% Creazione vettore dei termini noti
b = sum(A')';

% Risoluzione sistema
x = risolvi_sistema(A, b);

% Errore relativo
er = norma(ones(10, 1) - x, 1) / norma(ones(10, 1), 1); 
ic = ind_cond(A, 1);

fprintf('Indice cond: %e\n', ic);

fprintf('Sistema originale: errore relativo: %e\n', er);

% Perturbazione
b(1) = b(1) * 1.01;

% Risoluzione sistema perturbato
newx = risolvi_sistema(A, b);

% Errore relativo tra la vecchia soluzione e la nuova
er = norma(x - newx, 1) / norma(x, 1); 
ic = ind_cond(A, 1);

fprintf('Sistema perturbato: errore relativo: %e\n', er);