function [x] = risolvi_sistema(A, b)
	[L, R, P] = gauss(A);
	x = bsub(R, fsub(L, P * b));
end
