clear all
close all
clc

% Creazione matrice A
A = [10 1 4 0; 1 10 5 -1; 4 5 10 7; 0 -1 7 9];

% Creazione vettore b
b = [15 15 26 15]';

% Indice di condizionamento
ic = ind_cond(A, 1);
fprintf('Indice di condizionamento: %e\n', ic);

% Risoluzione sistema
x = risolvi_sistema(A, b);

% Perturbazione
bp = b;
bp(1) = bp(1) * 1.01;

% Risoluzione sistema perturbato
newx = risolvi_sistema(A, bp);

% Errore relativo sul termine noto
er1 = norma(b - bp, 1) / norma(b, 1);

% Errore relativo sulla soluzione
er2 = norma(ones(4, 1) - x, 1) / norma(ones(4, 1), 1); 

fprintf('Errore relativo sul termine noto: %e\n', er1);
fprintf('Errore relativo sulla soluzione: %e\n', er2);