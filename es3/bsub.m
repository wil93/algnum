function [b] = bsub(R, b)
	for i = length(b) : -1 : 1
		b(i) -= R(i, i+1:end) * b(i+1:end);
		b(i) /= R(i, i);
	end
end
