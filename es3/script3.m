clc
clear all
close all

for n = 2 : 15
    % Creazione matrice di Vandermonde
    %A = vanderm(n, 1, 100); %
    A = vanderm(n, 0, 1);  
    
    % Creazione vettore dei termini noti
    b = sum(A')';
    
    % Risoluzione del sistema Ax = b
    x = risolvi_sistema(A, b);
    
    % Errore relativo
    er = norma(ones(n, 1) - x, 1) / norma(ones(n, 1), 1); 
    ic = ind_cond(A, 1);
    
    fprintf('%d\t%e\t%e\n', n, er, ic);
end
    
    
