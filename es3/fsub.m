function [b] = fsub(L, b)
	for i = 1 : length(b)
		b(i) -= L(i, 1:i-1) * b(1:i-1);
		b(i) /= L(i, i);
	end
end
