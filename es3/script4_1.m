>>> A = hilb(10) + eye(10);          % Costruzione matrice
>>> b = sum(A')';                    % Costruzione vettore termini noti
>>> K = ind_cond(A, 1)               % Calcolo indice di condizionamento 
K =  5.82153309426294                % usando la norma 1.
>>> x = risolvi_sistema(A, b)        % Risoluzione sistema
>>> errs = norma(ones(10, 1) - x, 1) / norma(ones(10, 1), 1)
errs =  2.22044604925031e-16         % Err. soluzione
