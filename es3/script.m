close all
clear all
clc

% (A) Creazione matrice di Wilkinson
fprintf('Matrice di Wilkinson di ordine 12:\n');
A = wilk(12);

% (B) Visualizzazione struttura
spy(A)

% (C) Fattorizzazione di Gauss a pivotaggio massimo
[L, R, P] = fatt_lu(A);

% (D) Perturbazione del 5%
Rp = R;
Rp(end, end) = Rp(end, end) * 1.05;

% (E) Stima dell'errore sul prodotto
Ap = L * Rp;
er = err_rel(Ap(end, end), A(end, end));

fprintf('%e\n', er);
