>>> bp = b;
>>> bp(1) *= 1.01;                   % Perturbazione del termine noto
>>> xp = risolvi_sistema(A, bp);
>>> errp = norma(ones(10, 1) - xp, 1) / norma(ones(10, 1), 1)
errp =  0.00431273606515443          % Err. soluzione:     0.43%
>>> errb = norma(b - bp, 1) / norma(b, 1)
errb =  0.0100000000000000           % Err. termine noto:  1.00%
