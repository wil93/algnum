close all;
clear all;
clc;

plotSize = '-S700,400';

for n = [5, 10, 15]
	x = linspace(-1, +1, n);
	f = @(x) 1/(1+25*x*x);
	%~ f = @(x) abs(x);
	y = arrayfun(f, x);
	m = 1000;
	x0 = linspace(-1, +1, m);
	y1 = zeros(size(x0));
	d  = differenze_div(x, y);
	for i = 1 : m
		y1(i) = newton_interp(x, d, x0(i));
	end
	figure;
	hold on;
	axis([-1, +1, -3, +3]);
	plot(x, y, 'o', x0, arrayfun(f, x0), 'b', x0, y1, 'r');
	legend('', 'Funzione originale', 'Newton');
	print('-dtex', strcat('plot1_', num2str(n)), plotSize);
	system(cstrcat('sed s/"plot[0-9]\+"/"..\/es6\/&"/g -i ', 'plot1_', num2str(n), '.tex'));
	figure;
	hold on;
	axis([-1, +1, -3, +3]);
	plot([-1, +1], [0, 0], 'k', x0, abs(arrayfun(f, x0) - y1), 'r');
	legend('', 'Errore rispetto alla f. originale');
	print('-dtex', strcat('plot2_', num2str(n)), plotSize);
	system(cstrcat('sed s/"plot[0-9]\+"/"..\/es6\/&"/g -i ', 'plot2_', num2str(n), '.tex'));
end

for n = [5, 10, 15]
	x = arrayfun(@(i) cos(pi*(2*i-1)/(2*n)), 1 : n);
	f = @(x) 1/(1+25*x*x);
	%~ f = @(x) abs(x);
	y = arrayfun(f, x);
	m = 1000;
	x0 = linspace(-1, +1, m);
	y1 = zeros(size(x0));
	d  = differenze_div(x, y);
	for i = 1 : m
		y1(i) = newton_interp(x, d, x0(i));
	end
	figure;
	hold on;
	axis([-1, +1, -3, +3]);
	plot(x, y, 'o', x0, arrayfun(f, x0), 'b', x0, y1, 'r');
	legend('', 'Funzione originale', 'Newton');
	print('-dtex', strcat('plot3_', num2str(n)), plotSize);
	system(cstrcat('sed s/"plot[0-9]\+"/"..\/es6\/&"/g -i ', 'plot3_', num2str(n), '.tex'));
	figure;
	hold on;
	axis([-1, +1, -3, +3]);
	plot([-1, +1], [0, 0], 'k', x0, abs(arrayfun(f, x0) - y1), 'r');
	legend('', 'Errore rispetto alla f. originale');
	print('-dtex', strcat('plot4_', num2str(n)), plotSize);
	system(cstrcat('sed s/"plot[0-9]\+"/"..\/es6\/&"/g -i ', 'plot4_', num2str(n), '.tex'));
end
