close all;
clear all;
clc;

% Inserimento punti
n = input('Numero di punti: ');
figure;
hold on;
axis([0, 1, 0, 1]);
[x, y] = ginput(n);
% Disegno dei punti
plot(x, y, 'o');
% Chiedo se e' il caso di interpolare con un polinomio
while 1
	scelta = input('Interpolare con polinomio o curva? {1/2}: ');
	if scelta == 1 || scelta == 2, break, end
end
if scelta == 1
	% Intervallo degli m punti da valutare
	m = 1000;
	x0 = linspace(0, 1, m);
	% Calcolo del polinomio interpolatore di Lagrange
	y0 = zeros(size(x0));
	for i = 1 : m
		y0(i) = lagrange(x, y, x0(i));
	end
	% Calcolo del polinomio interpolatore di Newton
	y1 = zeros(size(x0));
	d  = differenze_div(x, y);
	for i = 1 : m
		y1(i) = newton_interp(x, d, x0(i));
	end
	% Calcolo del polinomio interpolatore mediante spline
	y2 = zeros(size(x0));
	for i = 1 : m
		y2(i) = spline(x, y, x0(i));
	end
	plot(x0, y0, 'g', x0, y1, 'r', x0, y2, 'b');
	fprintf('asd\n');
	[x0; y0; y1; y2]
	legend('Punti', 'Lagrange', 'Newton', 'Spline');
else
	% Costruzione del vettore t
	t = zeros(size(x));
	for i = 2 : n
		t(i) = t(i-1) + norm([x(i) - x(i-1), y(i) - y(i-1)], 2);
	end
	t = t / t(n);
	% Costruzione del vettore x0
	x0 = linspace(0, 1, 1000);
	% Risolviamo i due problemi di interpolazione, con Newton
	Px = arrayfun(@(i) newton_interp(t, differenze_div(t, x), i), x0);
	Py = arrayfun(@(i) newton_interp(t, differenze_div(t, y), i), x0);
	% Risolviamo i due problemi di interpolazione, con le spline
	Sx = arrayfun(@(i) spline(t, x, i), x0);
	Sy = arrayfun(@(i) spline(t, y, i), x0);
	% Disegnamo la curva
	plot(Px, Py, 'r', Sx, Sy, 'g');
	legend('Punti', 'Newton', 'Spline');
end

asd = input(' ');
