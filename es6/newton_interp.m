function [y0] = newton_interp(x, d, x0)
	y0 = d(end);
	for i = (length(x)-1) : -1 : 1
		y0 = d(i) + (x0 - x(i)) * y0;
	end
end
