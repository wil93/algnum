function [d] = differenze_div(x, y)
	d = y;
	for i = 2 : length(x)
		for k = length(x) : -1 : i
			d(k) = (d(k) - d(k-1)) / (x(k) - x(k-i+1));
		end
	end
end
