close all;
clear all;
clc;

% Scelta di x(i)
while 1
	scelta1 = input('Punti equidistanti in [-1, +1] oppure zeri del polinomio di Chebishev? {1/2}: ');
	if scelta1 == 1 || scelta1 == 2, break, end
end

while 1
	scelta2 = input('Valutare con la funzione 1/(1+25*x*x) oppure con |x|? {1/2}: ');
	if scelta2 == 1 || scelta2 == 2, break, end
end

% Inserimento di n
n = input('Inserisci n: ');

% Creazione di x(i)
if scelta1 == 1
	% Valori equidistanti
	x = linspace(-1, +1, n);
else
	% Radici del polinomio di Chebishev
	x = arrayfun(@(i) cos(pi*(2*i-1)/(2*n)), 1 : n);
end
% Creazione di y(i)
if scelta2 == 1
	% Uso la prima funzione
	f = @(x) 1/(1+25*x*x);
else
	% Uso la seconda funzione
	f = @(x) abs(x);
end
% Applico la funzione ad ogni x(i)
y = arrayfun(f, x);
% Intervallo degli m punti da valutare
m = 1000;
x0 = linspace(-1, +1, m);
% Calcolo del polinomio interpolatore di Lagrange
tic;
y0 = zeros(size(x0));
for i = 1 : m
	y0(i) = lagrange(x, y, x0(i));
end
fprintf('Tempo di esecuzione di Lagrange: %.15f\n', toc);
% Calcolo del polinomio interpolatore di Newton
tic;
y1 = zeros(size(x0));
d  = differenze_div(x, y);
for i = 1 : m
	y1(i) = newton_interp(x, d, x0(i));
end
fprintf('Tempo di esecuzione di Newton: %.15f\n', toc);
% Disegno dei grafici richiesti
figure(n);
hold on;
axis([-1, +1, -3, +3]);
plot(x, y, 'o', x0, arrayfun(f, x0), 'b', x0, y0, 'g', x0, y1, 'r');
legend('Punti', 'Funzione originale', 'Lagrange', 'Newton');
% Disegno dei grafici degli errori
figure(1);
hold on;
axis([-1, +1, -3, +3]);
plot(x, 0, 'ob', x0, abs(arrayfun(f, x0) - y0), 'g', x0, abs(arrayfun(f, x0) - y1), 'r');
