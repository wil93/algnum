function [y0] = lagrange(x, y, x0)
	y0 = 0;
	for i = 1 : length(x)
		p = y(i);
		for j = 1 : length(x)
			if i ~= j
				p *= (x0 - x(j)) / (x(i) - x(j));
			end
		end
		y0 += p;
	end
end
