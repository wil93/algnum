% Script di testing per gli esercizi di algoritmi numerici
% William Di Luigi

% TEST 1
fprintf('\n##########################################################');
fprintf('\n## TEST 1: Precisione di macchina\n\n');
fprintf('Precisione calcolata:     %e\n', precisione());
fprintf('Funzione eps() di OCTAVE: %e\n', eps());

% TEST 2
coefficienti = [5 2 0 0 -3 20 1 -1];
punti = [0 0.37 1];

fprintf('\n##########################################################');
fprintf('\n## TEST 2: Valutazione di un polinomio\n\n');
fprintf('Polinomio: p(x) = ');
fprintf('%+ix^%i', [coefficienti; length(coefficienti)-1 : -1 : 0]);
fprintf('\nNei punti: ');
fprintf('x = %e; ', punti);

fprintf('\n\nValutazione del polinomio con lo schema ingenuo:\n');
tic;
valuta = valuta_ingenuo(coefficienti, punti);
toc;
fprintf('p(%e) = %e\n', [punti; valuta]);

fprintf('\nValutazione del polinomio con lo schema di Horner:\n');
tic;
valuta = valuta_horner(coefficienti, punti);
toc;
fprintf('p(%e) = %e\n', [punti; valuta]);

fprintf('\nValutazione del polinomio con la funzione polyval() di OCTAVE:\n');
tic;
valuta = polyval(coefficienti, punti);
toc;
fprintf('p(%e) = %e\n', [punti; valuta]);

% TEST 3
punti = [1 60 80 -1 -60 -80];

fprintf('\n##########################################################');
fprintf('\n## TEST 3: Funzione esponenziale\n\n');
fprintf('Calcolata nei seguenti punti:\n');
fprintf('x = %e;\n', punti);

fprintf('\nCalcolo di e^x con un algoritmo non stabile:\n');
fprintf('e^(%e) = %e\n', [punti; arrayfun(@exp_instabile, punti)]);

fprintf('\nCalcolo di e^x con un algoritmo stabile:\n');
fprintf('e^(%e) = %e\n', [punti; arrayfun(@exp_stabile, punti)]);

fprintf('\nCalcolo di e^x con la funzione exp() di OCTAVE:\n');
fprintf('e^(%e) = %e\n', [punti; exp(punti)]);
