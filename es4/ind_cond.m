function [y] = ind_cond(A, p)
	y = norma(A, p) * norma(inv(A), p);
end
