>>> A =  diag(2 * ones(1, n));       % Diagonale principale
>>> A += diag(-ones(1, n-1), 1);     % Codiagonale superiore
>>> A += diag(-ones(1, n-1), -1);    % Codiagonale inferiore
