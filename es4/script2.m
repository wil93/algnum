% Costruzione della matrice B a partire da una matrice casuale
A = rand(3000);
B = A' * A;

ind_cond(B,1)

% Verifica se B e' definita positiva
[~, possible] = cholesky(B);

% Se lo e', allora...
if possible > 0
    % Fattorizziamo con Cholesky e misuriamo il tempo
    tic;
    [L] = cholesky(B);
    t1 = toc;
    % Prepariamo il vettore dei termini noti t
    t = sum(B)';
    % Forward substitution su L
    y = fsub(L, t);
    % Backward substitution su L trasposta
    x = bsub(L', y)

    % Fattorizziamo con Gauss e misuriamo il tempo
    tic;
    [L, R, P] = gauss(B);
    t2 = toc;
    % Prepariamo il vettore dei termini noti t
    t = sum(B)';
    % Forward substitution su L
    y = fsub(L, P*t);
    % Backward substitution su R
    x = bsub(R, y)

    % Stampa dei tempi
    fprintf('%.15f\t%.15f\n', t1, t2);
end

