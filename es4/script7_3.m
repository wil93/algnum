>>> tic;                          % Avviamo il cronometro
>>> [L R P] = gauss(B);           % Fattorizziamo con Gauss
>>> toc                           % Fermiamo il cronometro
Elapsed time is 117.81 seconds.

>>> b = sum(P * B)';              % Vettore termini noti
>>> y = fsub(L, b);               % Sostituzione in avanti su L
>>> x = bsub(R, y);               % Sostituzione all'indietro su L'
>>> x'
ans =
   1.00000   1.00000   1.00000   1.00000   1.00000   1.00000 ...
