% a (n), b (n-1), c (n-1)
function [x] = solve(a, b, c, t)
	[alpha, beta] = fatt_tri(a, b, c);
	n = length(a);
	y = zeros(n, 1);
	y(1) = t(1);
	for i = 2 : n
		y(i) = t(i) - beta(i-1) * y(i-1);
	end
	x = zeros(n, 1);
	x(n) = y(n) / alpha(n);
	for i = n-1 : -1 : 1
		x(i) = (y(i) - c(i) * x(i+1)) / alpha(i);
	end
end
