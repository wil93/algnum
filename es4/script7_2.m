>>> tic;                          % Avviamo il cronometro
>>> [L] = cholesky(B);            % Fattorizziamo con Cholesky
>>> toc                           % Fermiamo il cronometro
Elapsed time is 11.663 seconds.

>>> b = sum(B)';                  % Vettore termini noti
>>> y = fsub(L, b);               % Sostituzione in avanti su L
>>> x = bsub(L', y);              % Sostituzione all'indietro su L'
>>> x'
ans =
   1.00000   1.00000   1.00000   1.00000   1.00000   1.00000 ...
