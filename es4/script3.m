% Costruzione di una matrice simmetrica definita positiva
% seguendo l'algoritmo esposto anche nella relazione inviata

% (1) Matrice random
A = rand(5);

% (2) Estrazione della matrice triangolare inferiore
A = tril(A);

% (3) Completamento a matrice simmetrica aggiungendo la trasposta
A = A + A';

% (4) Modifica degli elementi della diagonale (aggiungendogli la
%     somma di tutti gli elementi nella stessa riga aumentata di 1)
A = A + diag(sum(abs(A')) + 1);

% Creazione del vettore dei termini noti b
b = sum(A');

% Fattorizzazione con Cholesky
[L] = cholesky(A);

% Risoluzione mediante forward & backward substitution
x = bsub(L', fsub(L, b));