function [alpha, beta] = gauss_tri(a, b, c)
	n = length(a);
	alpha = zeros(n, 1);
	beta = zeros(n-1, 1);
	alpha(1) = a(1);
	for i = 2 : n
		beta(i-1) = b(i-1) / alpha(i-1);
		alpha(i) = a(i) - beta(i-1) * c(i-1);
	end
end
