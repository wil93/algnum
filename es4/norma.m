function [y] = norma(A, p)
	if p
		y = max(sum(abs(A)));
	else
		y = max(sum(abs(A')));
	end
end
