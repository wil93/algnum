function [L, pos_def] = cholesky(A)
	pos_def = 1;
	n = length(A);
	L = zeros(n);
	for j = 1 : n
		rad = A(j, j) - L(j, 1:j-1) * L(j, 1:j-1)';
		if rad < 0
			pos_def = 0;
			break;
		end
		L(j, j) = sqrt(rad);
		L(j+1:end,j) = (A(j+1:end, j) - L(j+1:end, 1:j-1) * ...
		               L(j, 1:j-1)') / L(j, j);
	end
end
