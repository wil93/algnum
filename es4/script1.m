for n = [10, 50, 100, 250, 1000, 1500]
    % Costruzione delle 3 diagonali che rappresentano A
    a = 2 * ones(n, 1);
    b = -ones(n-1, 1);
    c = -ones(n-1, 1);

    % Costruzione di A
    A = diag(a) + diag(b, -1) + diag(c, 1);

    % Costruzione del vettore dei termini noti t
    t = sum(A);

    % Calcolo dei tempi
    tic;
    fatt_pieno(A);
    t1 = toc;
    tic;
    fatt_tri(a, b, c);
    t2 = toc;
    
    % Stampa dei tempi
    fprintf('%d\t%.15f\t%.15f\n', n, t2, t1);
end
