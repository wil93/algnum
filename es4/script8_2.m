>>> b = sum(A')';             % Termini noti
>>> [L] = cholesky(A)         % Fattorizzazione di Cholesky
L =
   1.23677   0.00000   0.00000   0.00000   0.00000
   0.05700   1.57652   0.00000   0.00000   0.00000
   0.13686   0.05192   2.28105   0.00000   0.00000
   0.30781   0.31277   0.29830   1.88478   0.00000
   0.62912   0.31334   0.27420   0.28003   1.89610
   
>>> y = fsub(L, b);           % Sostituzione in avanti su L
>>> y'
ans =
   2.3676   2.2546   2.8536   2.1648   1.8961
   
>>> x = bsub(L', y);          % Sostituzione all'indietro su L trasposta
>>> x'
ans =
   1.00000   1.00000   1.00000   1.00000   1.00000
   
