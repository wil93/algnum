>>> A = rand(3000);
>>> B = A' * A;                    % Costruzione di B
>>> [~, pos_def] = cholesky(B);    % B è definita positiva?
pos_def = 1                        % Si !
