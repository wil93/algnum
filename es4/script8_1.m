>>> A = rand(5);              % A matrice casuale 5x5
>>> A = tril(A);              % A matrice diagonale inferiore
>>> A += A';                  % A matrice simmetrica
>>> A += diag(sum(A))         % A matrice simmetrica e definita positiva
A =

   1.529600   0.070491   0.169268   0.380691   0.778072
   0.070491   2.488659   0.089647   0.510634   0.529851
   0.169268   0.089647   5.224607   0.738810   0.727845
   0.380691   0.510634   0.738810   3.833937   0.901238
   0.778072   0.529851   0.727845   0.901238   4.242784

