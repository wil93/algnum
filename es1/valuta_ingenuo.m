function [y] = valuta_ingenuo(a, x)
	y = 0; m = 1;
	for i = length(a) : -1 : 1
		y += m * a(i);
		m *= x;
	end
end
