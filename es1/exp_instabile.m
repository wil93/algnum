function [y] = exp_instabile(x)
	y = 0; f = 1; m = 1;
	for i = 1 : 150
		y += m / f;
		m *= x;
		f *= i;
	end
end
