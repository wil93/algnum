% Script di prova dei tempi di valutazione dei polinomi
clear all;
close all;
clc;

% Polinomio 1: p(x) = x^6 - 6x^5 + 15x^4 - 20x^3 + 15x^2 - 6x + 1
c1 = [1 -6 15 -20 15 -6 1];
p1 = linspace(0.9, 1.1, 1000);

% Polinomio 2: p(x) = x^8 - 8x^7 + 28x^6 - 7x^4 + 12x^3 - 4x + 1
c2 = [1 -8 28 0 -7 12 0 -4 1];
p2 = linspace(-1, 1, 1000);

figure;
plot(p1, arrayfun(@(x) valuta_horner(c1, x), p1), 'linewidth', 2);
print('-dtex', 'plot01', '-S700,400');

figure;
plot(p2, arrayfun(@(x) valuta_horner(c2, x), p2), 'linewidth', 2);
print('-dtex', 'plot02', '-S700,400');

% fix
system('sed s/"plot[0-9]\+"/"..\/es1\/&"/g -i plot01.tex');
system('sed s/"plot[0-9]\+"/"..\/es1\/&"/g -i plot02.tex');

fprintf("\nPolinomio 1\n");

% Valutazione con l'alg. ingenuo
fprintf("\nAlgoritmo ingenuo\n");
tic;
for i = 1:length(p1)
	valuta_ingenuo(c1, p1(i));
end
time = toc;
fprintf("tempo: %.15f\n", time);

fprintf("\nAlgoritmo di Horner\n");
tic;
for i = 1:length(p1)
	valuta_horner(c1, p1(i));
end
time = toc;
fprintf("tempo: %.15f\n", time);

fprintf("\nPolinomio 2\n");

% Valutazione con l'alg. ingenuo
fprintf("\nAlgoritmo ingenuo\n");
tic;
for i = 1:length(p2)
	valuta_ingenuo(c2, p2(i));
end
time = toc;
fprintf("tempo: %.15f\n", time);

fprintf("\nAlgoritmo di Horner\n");
tic;
for i = 1:length(p2)
	valuta_horner(c2, p2(i));
end
time = toc;
fprintf("tempo: %.15f\n", time);
