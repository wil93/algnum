function [e] = err_rel(a, b)
	e = abs((a - b) / a);
end
