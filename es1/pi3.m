function [r] = pi3(n);
	r = 0;
	for i = 1 : n
		if mod(i, 2)
			r += 4 / (2 * i - 1);
		else
			r -= 4 / (2 * i - 1);
		end
	end
end


