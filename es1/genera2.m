% script di test dell'esercizio 6
j = 1;
for i = [.1,.01,.008,.007,.005,.003]
	figure;
	hold on;
	sup = p2(1+i);
	if sup < 1
		c = 0;
		while 1
			sup *= 10;
			c ++;
			if sup >= 1, break, end
		end
		sup = floor(sup);
		while c
			sup /= 10;
			c --;
		end
	end
	axis([1-i, 1+i, -.1*p2(1+i), sup]);
	if i == .008
		set(gca,'YTick',[0, 4e-14, 1e-13, 2e-13])
	end
	I = linspace(1-i, 1+i, 100);
	plot(linspace(1-i, 1+i, 100), arrayfun(@(x) p1(x), linspace(1-i, 1+i, 100)), 'linewidth', 3, 'r');
	plot(linspace(1-i, 1+i, 100), arrayfun(@(x) p2(x), linspace(1-i, 1+i, 100)), 'linewidth', 3, 'b');
	plot(linspace(1-i, 1+i, 100), arrayfun(@(x) valuta_horner([1 -6 15 -20 15 -6 1], x), linspace(1-i, 1+i, 100)), 'linewidth', 3, 'g');
	legend('p1', 'p2', 'horner');
	print('-dtex', strcat('plot', num2str(j)), '-S700,500'); # grandezza plot: 1000 x 360

	# fixa il percorso al file eps/pdf
	system(cstrcat('sed s/"plot[0-9]\+"/"..\/es1\/&"/g -i ', 'plot', num2str(j), '.tex'));
	j += 1;
end
