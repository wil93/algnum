function [r] = pi2(n);
	c(1) = 0; p(1) = 2;
	for i = 2 : n
		c(i) = sqrt((1 + c(i-1)) / 2);
		p(i) = p(i-1) / c(i);
	end
	r = p(n);
end
