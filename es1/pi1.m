function [r] = pi1(n);
	b(1) = 2; s(1) = 1; r = 0;
	for i = 2 : n
		r = b(i-1) * s(i-1);
		b(i) = 2 * b(i-1);
		s(i) = sqrt((1-sqrt(1-s(i-1)^2))/2);
	end
end
