function [F] = derivate(f, x0, h)
	F = (f(x0 + h) - f(x0)) / h;
end
