function [epsilon] = precisione()
	epsilon = 1;
	while 1 + epsilon / 2 > 1
		epsilon /= 2;
	end
end
