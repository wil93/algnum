% script di test di pi-greco
clear all
close all
clc

n_max = 50;
figure;
hold on;
xlabel('Numero di iterazioni');
ylabel('Errore relativo');
plot([3:n_max], arrayfun(@(i) err_rel(pi1(i), pi), [3:n_max]), 'linewidth', 3, 'r');
plot([3:n_max], arrayfun(@(i) err_rel(pi2(i), pi), [3:n_max]), 'linewidth', 3, 'g');
plot([3:n_max], arrayfun(@(i) err_rel(pi3(i), pi), [3:n_max]), 'linewidth', 3, 'b');
plot([3:n_max], arrayfun(@(i) err_rel(pi4(i), pi), [3:n_max]), 'linewidth', 3, 'm');
legend('Archimede', 'Viete', 'Leibniz', 'Arcoseno');
print('-dtex', strcat('plot', num2str(7)), '-S700,300'); # grandezza plot: 1000 x 360

# fixa il percorso al file eps/pdf
system(cstrcat('sed s/"plot[0-9]\+"/"..\/es1\/&"/g -i ', 'plot', num2str(7), '.tex'));
