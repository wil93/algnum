function [r] = pi4(n);
	r = 0;
	a = 1;   % factorial(i)
	b = 1;   % factorial(2 * i)
	c = 1;   % 4 ^ i
	d = 1/2; % (1/2) ^ (2 * i + 1)
	for i = 0 : n
		r += b / (c * a * a * (2 * i + 1)) * d;
		a *= i+1;
		b *= 4*i*i + 6*i + 2;
		c *= 4;
		d /= 4;
	end 
	r *= 6;
end
