% Test calcolo della funzione esponenziale 
clear all
close all
clc

% Valori da valutare
val = [10 -20 -30 -40 -50];

for i = 1 : length(val)
	fprintf("%d,%e,%e,%e,%e,%e\n", val(i), exp(val(i)), exp_instabile(val(i)), err_rel(exp(val(i)), exp_instabile(val(i))),  exp_stabile(val(i)), err_rel(exp(val(i)), exp_stabile(val(i))));
end
