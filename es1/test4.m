% script di test per es. 4 (derivate)
close all
clear all
clc

% punti di derivazione
x1 = 1;
x2 = pi/6;

fprintf('a,b,c,d\n');
%~ 
%~ for i = 1 : 16
	%~ h = 10 ^ -i;
	%~ d = derivate(@f1, x1, h);
	%~ a = F1(x1);
	%~ fprintf("%.0e,%.5f,%.5f,%.2e\n", h, d, a, err_rel(d, a));
%~ end
for i = 1 : 16
	h = 10 ^ -i;
	d = derivate(@f2, x2, h);
	a = F2(x2);
	fprintf("%.0e,%.5f,%.5f,%.2e\n", h, d, a, err_rel(d, a));
end
