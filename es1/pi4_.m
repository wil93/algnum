%~ FILE VECCHIO, NON USARE
function [r] = pi4_(n);
	r = 0;
	for i = 0 : n
		r += factorial(2 * i) / ((4 ^ i) * (factorial(i) ^ 2) * (2 * i + 1)) * (1/2) ^ (2 * i + 1);
	end 
	r *= 6;
end
