function [y] = exp_stabile(x)
	if x < 0
		y = 1 / exp_stabile(-x);
		return;
	end
	y = 0; f = 1; m = 1;
	for i = 1 : 150
		y += m / f;
		m *= x;
		f *= i;
	end
end
