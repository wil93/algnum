function [y] = p1(x)
	y = x^6 -6 * x^5 + 15 * x^4 - 20 * x^3 + 15 * x^2 - 6 * x + 1;
end
