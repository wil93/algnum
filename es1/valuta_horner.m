function [y] = valuta_horner(a, x)
	y = 0;
	for i = 1 : length(a)
		y = y * x + a(i);
	end
end
