clear all;
close all;
clc;
format long;

i = zeros(7, 2);
exact = zeros(7, 1);

% Definizione funzioni
f{1} = @(x) 2^x;
i(1, :) = [0 4];
exact(1) = 15 / log(2);
f{2} = @(x) 1/(1+x);
i(2, :) = [1 2];
exact(2) = log(3 / 2);
f{3} = @(x) (1+x)/(1+x*x*x);
i(3, :) = [0 1];
exact(3) = 2*pi / (3*sqrt(3));
f{4} = @(x) 1/(1+x*x);
i(4, :) = [-1 1];
exact(4) = pi / 2;
f{5} = @(x) 3*x*x*x + 2*x + 2;
i(5, :) = [2 3];
exact(5) = 223 / 4;
f{6} = @(x) exp(x*x);
i(6, :) = [0 1];
exact(6) = 1.462651745907181608804048586856988155120870096216739185660114580218763314290979170821899812717535161;
f{7} = @(x) sin(10*x);
i(7, :) = [0 2];
exact(7) = sin(10) * sin(10) / 5;

%~ fprintf('a,b,c,d,e,f\n');
% Calcolo valore integrali
for j = 1 : 7
    A_t(j) = trapezi(i(j, 1), i(j, 2), f{j});
    A_s(j) = simpson(i(j, 1), i(j, 2), f{j});
    esatto = exact(j);
    %~ fprintf('%d,%.10f,%.10f,%e,%.10f,%e\n', j, esatto, A_t(j), err_rel(esatto, A_t(j)), A_s(j), err_rel(esatto, A_s(j)));
end

I = zeros(7, 3);
N = zeros(7, 2);

% Stima di Richardson
for j = 1 : 7
    E = 1; n = 1; p = 1; prec = 1e-5;
    I(j, 2) = trapezi_c(i(j, 1), i(j, 2), f{j}, n);
    while (E >= prec)
        n = n * 2;
        I(j, 1) = trapezi_c(i(j, 1), i(j, 2), f{j}, n);
        E  = (2^(n+p))/(2^(n+p)-1) * abs(I(j, 1) - I(j, 2));
        I(j, 2) = I(j, 1);
    end
    N(j, 1) = n;
end

for j = 1 : 7
    E = 1; n = 2; p = 2; prec = 1e-5;
    I(j, 3) = simpson_c(i(j, 1), i(j, 2), f{j}, n);
    while (E >= prec)
        n = n * 2;
        I(j, 1) = simpson_c(i(j, 1), i(j, 2), f{j}, n);
        E  = (2^(n+p))/(2^(n+p)-1) * abs(I(j, 1) - I(j, 3));
        I(j, 3) = I(j, 1);
    end
    N(j, 2) = n;
end

fprintf('a,b,c,d,e,f,g,h,i\n');
for j = 1 : 7
    esatto = exact(j);
    fprintf('%d,%.10f,%.10f,%d,%e,%.10f,%d,%e,%e\n', ...
        j, esatto, I(j, 2), N(j, 1), err_rel(esatto, I(j, 2)), I(j, 3), ...
        N(j, 2), err_rel(esatto, I(j, 3)), err_rel(I(j, 2), I(j, 3)));
end
