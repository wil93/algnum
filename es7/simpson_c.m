function [I] = simpson_c(a, b, f, k)
	I = 0;
	for i = 1 : k
		I += simpson(a+(b-a)*(i-1)/k, a+(b-a)*i/k, f);
	end
end
