function [I] = trapezi(a, b, f)
	I = (b-a) * (f(a) + f(b)) / 2;
end
