function [er] = err_rel(x1, x2)
    er = (x1 - x2) / x2;
    if (er < 0) 
        er = -er;
    end
end