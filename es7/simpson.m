function [I] = simpson(a, b, f)
	I = (b-a) * (f(a) + 4 * f((a+b) / 2) + f(b)) / 6;
end
