function [x] = secanti(f, x0, x1, eps, it)
	n = 2;
	x = [x0 x1];
	while (abs(x(n) - x(n-1)) > eps && n < it)
		x(n+1) = x(n) - f(x(n)) * (x(n)-x(n-1)) / (f(x(n))-f(x(n-1)));
		n ++;
	end
end
