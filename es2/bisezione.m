function [c] = bisezione(f, a, b, eps, it)
	n = 1;
	while (abs(b-a) > eps && n <= it)
		c(n) = a + (b-a) / 2;
		if (f(a) * f(c(n)) < 0)
			b = c(n);
		else
			a = c(n);
		end
		n ++;
	end
end
