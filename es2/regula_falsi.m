function [c] = regula_falsi(f, a, b, eps, it)
	n = 1;
	x = a;
	c(1) = b;
	do
		c(n+1) = c(n) - (c(n) - x) / (f(c(n)) - f(x)) * f(c(n));
		n ++;
	until abs(c(n) - c(n-1)) < eps || n == it
end
