clc
clear all
close all

% Data una funzione visualizza il suo grafico in un intervallo assegnato

I = [-3 2];
%~ I = input('Inserisci intervallo: ');

%~ figure('Units', 'normalized', 'Position', [0.3 0.3 0.4 0.4]);
%~ plot(linspace(I(1), I(2), 1000), arrayfun(@f, linspace(I(1), I(2), 1000)));

% Chiedi in input l'intervallo in cui cercare lo zero

I = [-1 3];
%~ repeat
%~ do
	%~ I = input('Intervallo in cui cercare lo zero: ');
%~ until I(1) < I(2)

precision = 1e-6;
%~ precision = input('Precisione desiderata: ');
%~ precision = eps;

max_ite = 100;
%~ repeat
%~ do
	%~ max_ite = input('Iterazioni massime: ');
%~ until max_ite > 0

% Calcola lo zero della funzione con il metodo della bisezione

figure('Units', 'normalized', 'Position', [0 0.5 0.5 0.4]);
title('Metodo di bisezione');
hold on;
plot(linspace(I(1), I(2), 1000), arrayfun(@f, linspace(I(1), I(2), 1000)));
plot([I(1) I(2)], [0 0], '-k');

ris1 = bisezione(@f, I(1), I(2), precision, max_ite);
for i = 1 : length(ris1)
	plot([ris1(i) ris1(i)], [0 f(ris1(i))], '-r.');
end

% Calcola lo zero della funzione con il metodo della regula falsi

figure('Units', 'normalized', 'Position', [0.5 0.5 0.5 0.4]);
title('Metodo della regula falsi');
hold on;
plot(linspace(I(1), I(2), 1000), arrayfun(@f, linspace(I(1), I(2), 1000)));
plot([I(1) I(2)], [0 0], '-k');

ris2 = regula_falsi(@f, I(1), I(2), precision, max_ite);
for i = 1 : length(ris2)
	plot([ris2(i) ris2(i)], [0 f(ris2(i))], '-r.');
	plot([ris2(1) ris2(i)], [f(ris2(1)) f(ris2(i))], '-g.');
end

% Calcola lo zero della funzione con il metodo di Newton

figure('Units', 'normalized', 'Position', [0 0 0.5 0.4]);
title('Metodo di Newton');
hold on;
plot(linspace(I(1), I(2), 1000), arrayfun(@f, linspace(I(1), I(2), 1000)));
plot([I(1) I(2)], [0 0], '-k');

ris3 = newton(@f, @df, I(2), precision, max_ite);
for i = 1 : length(ris3)
	plot([ris3(i) ris3(i)], [0 f(ris3(i))], '-r.');
	if i > 1
		plot([ris3(i-1) ris3(i)], [f(ris3(i-1)) 0], '-g.');
	end
end

% Calcola lo zero della funzione con il metodo delle secanti

figure('Units', 'normalized', 'Position', [0.5 0 0.5 0.4]);
title('Metodo delle secanti');
hold on;
plot(linspace(I(1), I(2), 1000), arrayfun(@f, linspace(I(1), I(2), 1000)));
plot([I(1) I(2)], [0 0], '-k');

ris4 = secanti(@f, I(2), precision, max_ite);
for i = 1 : length(ris4)
	plot([ris4(i) ris4(i)], [0 f(ris4(i))], '-r.');
	if i > 1
		plot([ris4(i-1) ris4(i)], [f(ris4(i-1)) f(ris4(i))], '-g.');
		if i+1 < length(ris4) && f(ris4(i)) * f(ris4(i-1)) > 0
			plot([ris4(i) ris4(i+1)], [f(ris4(i)) 0], '-g.');
		end
	end
end
