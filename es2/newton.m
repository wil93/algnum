function [x] = newton(f, df, x0, eps, it)
	x(1) = x0;
	n = 1;
	do
		n ++;
		x(n) = x(n-1) - f(x(n-1)) / df(x(n-1));
	until abs(x(n) - x(n-1)) < eps || n >= it
end
