% script di test per es. 4 (derivate)
close all

figure;
hold on;
axis([-2, 3, -3, 3]);
plot(linspace(-2, 3, 100), arrayfun(@(x) x*x*x-1.9*x*x-1.2*x+2.5, linspace(-2, 3, 100)), 'linewidth', 3);
plot([-2 3], [0 0], '-', 'color', 'k', 'linewidth', 2);
print('-dtex', 'plot8', '-S700,360'); # grandezza plot: 1000 x 360

# fixa il percorso al file eps/pdf
system(cstrcat('sed s/"plot[0-9]\+"/"..\/es2\/&"/g -i ', 'plot8.tex'));
